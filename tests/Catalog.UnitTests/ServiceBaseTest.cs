﻿using Catalog.BusinessLogic.Services;
using Catalog.Domain.Dependencies;
using Catalog.Domain.Exceptons;
using Catalog.Domain.Models;
using FluentAssertions;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Catalog.UnitTests
{
    public class ServiceBaseTest
    {
        [Fact]
        public async Task GetById_WhenNotExist_ShouldReturnNull()
        {
            // Arrange
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);

            var service = new CategoryService(repository.Object);

            // Act
            Func<Task> act = async () => await service.GetById(1);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 1 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Fact]
        public async Task GetById_WhenExists_ShouldReturnItem()
        {
            // Arrange
            var category = new Category
            {
                Id = 1,
                Name = "Category",
                ImageUrl = "Url",
                ParentCategoryId = null
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(category);

            var service = new CategoryService(repository.Object);

            // Act
            var result = await service.GetById(category.Id);

            // Assert
            result.Should().BeEquivalentTo(category);
        }

        [Fact]
        public async Task Add_WhenSpecified_ShouldAdd()
        {
            // Arrange
            var category = new Category
            {
                Id = 1,
                Name = "Category",
                ImageUrl = "Url",
                ParentCategoryId = null
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.Add(It.IsAny<Category>())).ReturnsAsync(category);

            var service = new CategoryService(repository.Object);

            // Act
            var result = await service.Add(category);

            // Assert
            result.Should().BeEquivalentTo(category);
            repository.Verify(x => x.Add(It.IsAny<Category>()), Times.Once);
        }

        [Fact]
        public async Task Update_WhenNoCategorySpecified_ShouldThrowException()
        {
            // Arrange
            var category = new Category
            {
                Id = 1,
                Name = "Category",
                ImageUrl = "Url",
                ParentCategoryId = null
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);

            var service = new CategoryService(repository.Object);

            // Act
            Func<Task> act = async () => await service.Update(category);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 1 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Fact]
        public async Task Update_WhenCategoryExists_ShouldUpdate()
        {
            // Arrange
            var category = new Category
            {
                Id = 1,
                Name = "Category",
                ImageUrl = "Url",
                ParentCategoryId = null
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.Update(It.IsAny<Category>())).ReturnsAsync(category);
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(category);

            var service = new CategoryService(repository.Object);

            // Act
            var result = await service.Update(category);

            // Assert
            result.Should().BeEquivalentTo(category);
            repository.Verify(x => x.Update(It.IsAny<Category>()), Times.Once);
        }

        [Fact]
        public async Task Delete_WhenNoCategorySpecified_ShouldThrowException()
        {
            // Arrange
            var category = new Category
            {
                Id = 1,
                Name = "Category",
                ImageUrl = "Url",
                ParentCategoryId = null
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);

            var service = new CategoryService(repository.Object);

            // Act
            Func<Task> act = async () => await service.Delete(1);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 1 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Fact]
        public async Task Deletes_WhenCategoryExists_ShouldUpdate()
        {
            // Arrange
            var category = new Category
            {
                Id = 1,
                Name = "Category",
                ImageUrl = "Url",
                ParentCategoryId = null
            };
            int sentIdToDelete = 0;
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.Delete(It.IsAny<int>())).Callback<int>(x => sentIdToDelete = x);
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(category);

            var service = new CategoryService(repository.Object);

            // Act
            await service.Delete(1);

            // Assert
            sentIdToDelete.Should().Be(1);
            repository.Verify(x => x.Delete(It.IsAny<int>()), Times.Once);
        }
    }
}
