using Catalog.BusinessLogic;
using Catalog.BusinessLogic.Services;
using Catalog.Domain.Dependencies;
using Catalog.Domain.Exceptons;
using Catalog.Domain.Models;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Catalog.UnitTests
{
    public class ProductServiceTest
    {
        [Fact]
        public async Task GetFilteredCount_WhenCategoryNotNullAndNotExists_ShouldThrowException()
        {
            // Arrange
            var repository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);
            
            var service = new ProductService(repository.Object, categoryRepository.Object);

            // Act
            Func<Task> act = async () => await service.GetFilteredCount(2);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task GetFilteredCount_WhenParameterNullable_ShouldReturnCount()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            var productRepository = new Mock<IRepository<Product>>();
            productRepository.Setup(x => x.GetAll()).Returns(products.AsQueryable());
            
            var service = new ProductService(productRepository.Object, categoryRepository.Object);

            // Act
            var result = await service.GetFilteredCount(null);

            // Assert
            result.Should().Be(3);
        }

        [Fact]
        public async Task GetFilteredCount_WhenParameterNotNullable_ShouldReturnCount()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 1,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            var productRepository = new Mock<IRepository<Product>>();
            productRepository.Setup(x => x.GetAll()).Returns(products.AsQueryable());
            
            var service = new ProductService(productRepository.Object, categoryRepository.Object);

            // Act
            var result = await service.GetFilteredCount(1);

            // Assert
            result.Should().Be(2);
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenCategoryNotNullAndNotExists_ShouldThrowException()
        {
            // Arrange
            var repository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);
            
            var service = new ProductService(repository.Object, categoryRepository.Object);

            // Act
            Func<Task> act = async () => await service.GetAllByCategoryIdWithPagination(2, 1, 1);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenHonestAndLastPage_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            var productRepository = new Mock<IRepository<Product>>();
            productRepository.Setup(x => x.GetAll()).Returns(products.AsQueryable());
            
            var service = new ProductService(productRepository.Object, categoryRepository.Object);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(null, 1, 3);

            // Assert
            result.Should().BeEquivalentTo(new List<Product>
            {
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                }
            });
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenNotHonestAndNotLastPage_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            var productRepository = new Mock<IRepository<Product>>();
            productRepository.Setup(x => x.GetAll()).Returns(products.AsQueryable());
            
            var service = new ProductService(productRepository.Object, categoryRepository.Object);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(null, 2, 1);

            // Assert
            result.Should().BeEquivalentTo(new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                }
            });
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenNotHonestAndLastPage_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            var productRepository = new Mock<IRepository<Product>>();
            productRepository.Setup(x => x.GetAll()).Returns(products.AsQueryable());
            
            var service = new ProductService(productRepository.Object, categoryRepository.Object);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(null, 2, 2);

            // Assert
            result.Should().BeEquivalentTo(new List<Product>
            {
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                }
            });
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenThePageMoreThenExisting_ShouldReturnEmptyCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            var productRepository = new Mock<IRepository<Product>>();
            productRepository.Setup(x => x.GetAll()).Returns(products.AsQueryable());
            
            var service = new ProductService(productRepository.Object, categoryRepository.Object);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(null, 2, 3);

            // Assert
            result.Should().BeEmpty();
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenCategorySpecified_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 7,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 7,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            var productRepository = new Mock<IRepository<Product>>();
            productRepository.Setup(x => x.GetAll()).Returns(products.AsQueryable());
            
            var service = new ProductService(productRepository.Object, categoryRepository.Object);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(7, 1, 2);

            // Assert
            result.Should().BeEquivalentTo(new List<Product>
            {
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 7,
                    Description = "Desc3",
                    Price = 5m
                }
            });
        }
        [Fact]
        public async Task Add_WhenParentCategoryInvalid_ShouldNotAdd()
        {
            // Arrange
            var product = new Product
            {
                Id = 1,
                Name = "Product1",
                ImageUrl = "Url1",
                Amount = 1,
                CategoryId = 2,
                Description = "Desc1",
                Price = 15m
            };
            var repository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);
            
            var service = new ProductService(repository.Object, categoryRepository.Object);

            // Act
            Func<Task> act = async () => await service.Add(product);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task Add_WhenParentCategoryExists_ShouldAdd()
        {
            // Arrange
            var product = new Product
            {
                Id = 1,
                Name = "Product1",
                ImageUrl = "Url1",
                Amount = 1,
                CategoryId = 2,
                Description = "Desc1",
                Price = 15m
            };
            var repository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            repository.Setup(x => x.Add(It.IsAny<Product>())).ReturnsAsync(product);
            
            var service = new ProductService(repository.Object, categoryRepository.Object);

            // Act
            var result = await service.Add(product);

            // Assert
            result.Should().BeEquivalentTo(product);
            repository.Verify(x => x.Add(It.IsAny<Product>()), Times.Once);
        }

        [Fact]
        public async Task Update_WhenParentCategoryInvalid_ShouldNotUpdate()
        {
            // Arrange
            var product = new Product
            {
                Id = 1,
                Name = "Product1",
                ImageUrl = "Url1",
                Amount = 1,
                CategoryId = 2,
                Description = "Desc1",
                Price = 15m
            };
            var repository = new Mock<IRepository<Product>>();
            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);
            
            var service = new ProductService(repository.Object, categoryRepository.Object);

            // Act
            Func<Task> act = async () => await service.Update(product);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task Update_WhenParentCategoryExists_ShouldAdd()
        {
            // Arrange
            var product = new Product
            {
                Id = 1,
                Name = "Product1",
                ImageUrl = "Url1",
                Amount = 1,
                CategoryId = 2,
                Description = "Desc1",
                Price = 15m
            };
            var categoryRepository = new Mock<IRepository<Category>>();
            categoryRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category());
            var productRepository = new Mock<IRepository<Product>>();
            productRepository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(product);
            productRepository.Setup(x => x.Update(It.IsAny<Product>())).ReturnsAsync(product);
            
            var service = new ProductService(productRepository.Object, categoryRepository.Object);

            // Act
            var result = await service.Update(product);

            // Assert
            result.Should().BeEquivalentTo(product);
            productRepository.Verify(x => x.Update(It.IsAny<Product>()), Times.Once);
        }
    }
}
