using Catalog.BusinessLogic;
using Catalog.BusinessLogic.Services;
using Catalog.Domain.Dependencies;
using Catalog.Domain.Exceptons;
using Catalog.Domain.Models;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Catalog.UnitTests
{
    public class CategoryServiceTest
    {
        [Fact]
        public async Task Add_WhenParentCategoryInvalid_ShouldNotAdd()
        {
            // Arrange
            var categoryToAdd = new Category
            {
                Id = 3,
                Name = "Category1",
                ImageUrl = "Url1",
                ParentCategoryId = 2
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);
            var service = new CategoryService(repository.Object);

            // Act
            Func<Task> act = async () => await service.Add(categoryToAdd);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task Add_WhenParentCategoryExists_ShouldAdd()
        {
            // Arrange
            var categoryToAdd = new Category
            {
                Id = 3,
                Name = "Category3",
                ImageUrl = "Url3",
                ParentCategoryId = null
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category
            {
                Id = 3,
                Name = "Category1",
                ImageUrl = "Url1",
                ParentCategoryId = null
            });
            repository.Setup(x => x.Add(It.IsAny<Category>())).ReturnsAsync(categoryToAdd);
            var service = new CategoryService(repository.Object);

            // Act
            var result = await service.Add(categoryToAdd);

            // Assert
            result.Should().BeEquivalentTo(categoryToAdd);
            repository.Verify(x => x.Add(It.IsAny<Category>()), Times.Once);
        }

        [Fact]
        public async Task Update_WhenParentCategoryInvalid_ShouldNotUpdate()
        {
            // Arrange
            var category = new Category
            {
                Id = 3,
                Name = "Category1",
                ImageUrl = "Url1",
                ParentCategoryId = 2
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync((Category)null);
            var service = new CategoryService(repository.Object);

            // Act
            Func<Task> act = async () => await service.Update(category);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task Update_WhenParentCategoryExists_ShouldAdd()
        {
            // Arrange
            var category = new Category
            {
                Id = 3,
                Name = "Category3",
                ImageUrl = "Url3",
                ParentCategoryId = null
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(new Category
            {
                Id = 3,
                Name = "Category1",
                ImageUrl = "Url1",
                ParentCategoryId = null
            });
            repository.Setup(x => x.Update(It.IsAny<Category>())).ReturnsAsync(category);
            var service = new CategoryService(repository.Object);

            // Act
            var result = await service.Update(category);

            // Assert
            result.Should().BeEquivalentTo(category);
            repository.Verify(x => x.Update(It.IsAny<Category>()), Times.Once);
        }

        [Fact]
        public async Task Delete_WhenThereAreLinkedCategories_ShouldNotDelete()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 3,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = 2
                }
            };
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetAll()).Returns(categories.AsQueryable());
            var service = new CategoryService(repository.Object);

            // Act
            Func<Task> act = async () => await service.Delete(2);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("There are categories that has the category as linked to.");
            exception.StatusCode.Should().Be(ErrorType.InstanceHasLinkedObjects);
        }

        [Fact]
        public async Task Delete_WhenParentCategoryExists_ShouldAdd()
        {
            // Arrange
            var category = new Category
            {
                Id = 3,
                Name = "Category3",
                ImageUrl = "Url3",
                ParentCategoryId = null
            };
            int sentToDelete = 0;
            var repository = new Mock<IRepository<Category>>();
            repository.Setup(x => x.GetAll()).Returns(new List<Category>().AsQueryable());
            repository.Setup(x => x.GetById(It.IsAny<int>())).ReturnsAsync(category);
            repository.Setup(x => x.Delete(It.IsAny<int>())).Callback<int>(x => sentToDelete = x);
            var service = new CategoryService(repository.Object);

            // Act
            await service.Delete(3);

            // Assert
            sentToDelete.Should().Be(3);
            repository.Verify(x => x.Delete(It.IsAny<int>()), Times.Once);
        }
    }
}
