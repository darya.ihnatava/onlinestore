using Cart.BusinessLogic;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Cart.Domain.Exceptions;
using Cart.Domain.Dependencies;

namespace Cart.UnitTests
{
    public class CartServiceTest
    {
        [Test]
        public void GetById_WhenExists_ShouldReturnTheCart()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var cart = new Domain.Models.Cart
            {
                Id = guid,
                Items = new List<Domain.Models.CartItem>
                    {
                        new Domain.Models.CartItem
                        {
                            Id = 4,
                            Name = "CartItem4",
                            ImageUrl = "Url4",
                            Price = 16m,
                            Quantity = 6
                        },
                        new Domain.Models.CartItem
                        {
                            Id = 5,
                            Name = "CartItem5",
                            ImageUrl = "Url5",
                            Price = 17.30m,
                            Quantity = 3
                        }
                    }
            };
            var repository = new Mock<ICartRepository>();
            repository.Setup(x => x.GetById(It.IsAny<Guid>())).Returns(cart);
            var service = new CartService(repository.Object);

            // Act
            var result = service.GetById(guid);

            // Assert
            result.Should().BeEquivalentTo(cart);
        }

        [Test]
        public void GetById_WhenNotExists_ShouldReturnNull()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var repository = new Mock<ICartRepository>();
            repository.Setup(x => x.GetById(It.IsAny<Guid>())).Returns((Domain.Models.Cart)null);
            var service = new CartService(repository.Object);

            // Act
            TestDelegate act = () => service.GetById(guid);

            // Assert
            var exception = Assert.Throws<ServiceException>(act);
            exception.Message.Should().Be($"There are no cart {guid}.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Test]
        public void AddItem_WhenNoSuchCart_ShouldCreateNew()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var cart = new Domain.Models.Cart
            {
                Id = guid,
                Items = new List<Domain.Models.CartItem>
                {
                    new Domain.Models.CartItem
                    {
                        Id = 1,
                        Name = "Cart",
                        ImageUrl = "htpp://url.com",
                        Price = 1m,
                        Quantity = 5
                    }
                }
            };
            var repository = new Mock<ICartRepository>();
            repository.Setup(x => x.GetById(It.IsAny<Guid>())).Returns((Domain.Models.Cart)null);
            repository.Setup(x => x.Create(It.IsAny<Domain.Models.Cart>())).Returns(cart);
            var service = new CartService(repository.Object);

            // Act
            var result = service.AddItem(guid, new Domain.Models.CartItem
            {
                Id = 1,
                Name = "Cart",
                ImageUrl = "htpp://url.com",
                Price = 1m,
                Quantity = 5
            });

            // Asserst
            result.Should().BeEquivalentTo(cart);
            repository.Verify(x => x.Create(It.IsAny<Domain.Models.Cart>()), Times.Once);
        }

        [Test]
        public void AddItem_WhenCartItemAlreadyExists_ShouldThrowExceptin()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var item = new Domain.Models.CartItem
            {
                Id = 1,
                Name = "Cart",
                ImageUrl = "htpp://url.com",
                Price = 1m,
                Quantity = 5
            };
            var cart = new Domain.Models.Cart
            {
                Id = guid,
                Items = new List<Domain.Models.CartItem> { item }
            };
            var repository = new Mock<ICartRepository>();
            repository.Setup(x => x.GetById(It.IsAny<Guid>())).Returns(cart);
            repository.Setup(x => x.Create(It.IsAny<Domain.Models.Cart>())).Returns(cart);
            var service = new CartService(repository.Object);

            // Act
            TestDelegate act = () => service.AddItem(guid, item);

            // Assert
            var exception = Assert.Throws<ServiceException>(act);
            exception.Message.Should().Be($"The item {item.Id} already exists in cart {cart.Id}.");
            exception.StatusCode.Should().Be(ErrorType.CollectionItemAlreadyExists);
        }

        [Test]
        public void AddItem_WhenNoSuchItemExists_ShouldAdd()
        {
            // Arrange
            var guid = Guid.NewGuid(); 
            var item = new Domain.Models.CartItem
            {
                Id = 2,
                Name = "CartItem2",
                ImageUrl = "htpp://url.com",
                Price = 10m,
                Quantity = 2
            };
            var cart = new Domain.Models.Cart
            {
                Id = guid,
                Items = new List<Domain.Models.CartItem>
                {
                    new Domain.Models.CartItem
                    {
                        Id = 1,
                        Name = "CartItem1",
                        ImageUrl = "htpp://url.com",
                        Price = 1m,
                        Quantity = 5
                    }
                }
            };
            var resultCart = new Domain.Models.Cart
            {
                Id = guid,
                Items = new List<Domain.Models.CartItem>
                {
                    new Domain.Models.CartItem
                    {
                        Id = 1,
                        Name = "CartItem1",
                        ImageUrl = "htpp://url.com",
                        Price = 1m,
                        Quantity = 5
                    },
                    item
                }
            };
            var repository = new Mock<ICartRepository>();
            repository.Setup(x => x.GetById(It.IsAny<Guid>())).Returns(cart);
            repository.Setup(x => x.Update(It.IsAny<Domain.Models.Cart>())).Returns(resultCart);
            var service = new CartService(repository.Object);

            // Act
            var result = service.AddItem(guid, item);

            // Asserst
            result.Should().BeEquivalentTo(resultCart);
            repository.Verify(x => x.Update(It.IsAny<Domain.Models.Cart>()), Times.Once);
        }

        [Test]
        public void RemoveItem_WhenNoSuchCart_ShouldReturnFalse()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var repository = new Mock<ICartRepository>();
            repository.Setup(x => x.GetById(It.IsAny<Guid>())).Returns((Domain.Models.Cart)null);
            var service = new CartService(repository.Object);

            // Act
            TestDelegate act = () => service.RemoveItem(guid, 1);

            // Assert
            var exception = Assert.Throws<ServiceException>(act);
            exception.Message.Should().Be($"There are no cart {guid}.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Test]
        public void RemoveItem_WhenItemNotExists_ShouldReturnFalse()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var cart = new Domain.Models.Cart
            {
                Id = guid,
                Items = new List<Domain.Models.CartItem>
                    {
                        new Domain.Models.CartItem
                        {
                            Id = 4,
                            Name = "CartItem4",
                            ImageUrl = "Url4",
                            Price = 16m,
                            Quantity = 6
                        },
                        new Domain.Models.CartItem
                        {
                            Id = 5,
                            Name = "CartItem5",
                            ImageUrl = "Url5",
                            Price = 17.30m,
                            Quantity = 3
                        }
                    }
            };
            var repository = new Mock<ICartRepository>();
            repository.Setup(x => x.GetById(It.IsAny<Guid>())).Returns(cart);
            var service = new CartService(repository.Object);

            // Act
            TestDelegate act = () => service.RemoveItem(guid, 1);

            // Assert
            var exception = Assert.Throws<ServiceException>(act);
            exception.Message.Should().Be($"There are no item 1 in cart {cart.Id}.");
            exception.StatusCode.Should().Be(ErrorType.CollectionItemNotExists);
        }

        [Test]
        public void RemoveItem_WhenItemExists_ShouldRemoveAndReturnTrue()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var cart = new Domain.Models.Cart
            {
                Id = guid,
                Items = new List<Domain.Models.CartItem>
                    {
                        new Domain.Models.CartItem
                        {
                            Id = 4,
                            Name = "CartItem4",
                            ImageUrl = "Url4",
                            Price = 16m,
                            Quantity = 6
                        },
                        new Domain.Models.CartItem
                        {
                            Id = 5,
                            Name = "CartItem5",
                            ImageUrl = "Url5",
                            Price = 17.30m,
                            Quantity = 3
                        }
                    }
            };
            var expectedCart = new Domain.Models.Cart
            {
                Id = guid,
                Items = new List<Domain.Models.CartItem>
                    {
                        new Domain.Models.CartItem
                        {
                            Id = 5,
                            Name = "CartItem5",
                            ImageUrl = "Url5",
                            Price = 17.30m,
                            Quantity = 3
                        }
                    }
            };
            var repository = new Mock<ICartRepository>();
            repository.Setup(x => x.GetById(It.IsAny<Guid>())).Returns(cart);
            repository.Setup(x => x.Update(It.IsAny<Domain.Models.Cart>())).Returns(expectedCart);
            var service = new CartService(repository.Object);

            // Act
            var result = service.RemoveItem(guid, 4);

            // Assert
            result.Should().BeEquivalentTo(expectedCart);
            repository.Verify(x => x.Update(It.IsAny<Domain.Models.Cart>()), Times.Once);
        }
    }
}