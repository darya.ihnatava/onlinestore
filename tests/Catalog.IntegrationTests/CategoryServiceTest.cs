﻿using Catalog.BusinessLogic.Services;
using Catalog.DataAccess;
using Catalog.Domain.Exceptons;
using Catalog.Domain.Models;
using Catalog.IntegrationTests.Core;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Catalog.IntegrationTests
{
    public class CategoryServiceTest
    {
        [Fact]
        public async Task GetAll_WhenExists_ShouldReturnCollection()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var fixture = GetFixture(categories); 
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            var result = await service.GetAll();

            // Assert
            result.Should().BeEquivalentTo(categories);
        }

        [Fact]
        public async Task GetAll_WhenNothingExists_ShouldReturnEmptyCollection()
        {
            // Arrange
            var fixture = GetFixture();
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            var result = await service.GetAll();

            // Assert
            result.Should().BeEmpty();
        }

        [Fact]
        public async Task GetById_WhenExists_ShouldReturn()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            var result = await service.GetById(2);

            // Assert
            result.Should().BeEquivalentTo(categories[1]);
        }

        [Fact]
        public async Task GetById_WhenNotExists_ShouldNull()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            Func<Task> act = async () => await service.GetById(3);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 3 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Fact]
        public async Task Add_WhenParentCategoryInvalid_ShouldNotAdd()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                }
            };
            var categoryToAdd = new Category
            {
                Id = 3,
                Name = "Category1",
                ImageUrl = "Url1",
                ParentCategoryId = 2
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            Func<Task> act = async () => await service.Add(categoryToAdd);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task Add_WhenParentCategoryExists_ShouldAdd()
        {
            // Arrange
            var categoryToAdd = new Category
            {
                Id = 3,
                Name = "Category3",
                ImageUrl = "Url3",
                ParentCategoryId = null
            };
            var fixture = GetFixture();
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            var result = await service.Add(categoryToAdd);

            // Assert
            result.Should().BeEquivalentTo(categoryToAdd);
        }

        [Fact]
        public async Task Update_WhenParentCategoryInvalid_ShouldNotUpdate()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var category = new Category
            {
                Id = 2,
                Name = "Category1",
                ImageUrl = "Url1",
                ParentCategoryId = 3
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            Func<Task> act = async () => await service.Update(category);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 3 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task Update_WhenCategoryInvalid_ShouldNotUpdate()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var category = new Category
            {
                Id = 3,
                Name = "Category1",
                ImageUrl = "Url1",
                ParentCategoryId = 1
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            Func<Task> act = async () => await service.Update(category);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 3 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }
        [Fact]
        public async Task Update_WhenParentCategoryExists_ShouldAdd()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var category = new Category
            {
                Id = 2,
                Name = "Category3",
                ImageUrl = "Url3",
                ParentCategoryId = 2
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            var result = await service.Update(category);

            // Assert
            result.Should().BeEquivalentTo(category);
        }

        [Fact]
        public async Task Delete_WhenThereAreLinkedCategories_ShouldNotDelete()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            Func<Task> act = async () => await service.Delete(1);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("There are categories that has the category as linked to.");
            exception.StatusCode.Should().Be(ErrorType.InstanceHasLinkedObjects);
        }

        [Fact]
        public async Task Delete_WhenCategoryNotExists_ShouldNotDelete()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            Func<Task> act = async () => await service.Delete(3);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 3 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Fact]
        public async Task Delete_WhenParentCategoryExists_ShouldAdd()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                },
                new Category
                {
                    Id = 2,
                    Name = "Category2",
                    ImageUrl = "Url2",
                    ParentCategoryId = 1
                }
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var repository = new Repository<Category>(dbContext);
            var service = new CategoryService(repository);

            // Act
            await service.Delete(2);
            var leftCategories = await service.GetAll();

            // Assert
            leftCategories.Should().BeEquivalentTo(new List<Category> 
            {
                new Category
                {
                    Id = 1,
                    Name = "Category1",
                    ImageUrl = "Url1",
                    ParentCategoryId = null
                }
            });
        }

        private SqlLiteFixture GetFixture(List<Category> categories = null)
        {
            return new SqlLiteFixture(categories);
        }
    }
}
