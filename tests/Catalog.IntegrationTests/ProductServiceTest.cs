﻿using Catalog.BusinessLogic.Services;
using Catalog.DataAccess;
using Catalog.Domain.Dependencies;
using Catalog.Domain.Exceptons;
using Catalog.Domain.Models;
using Catalog.IntegrationTests.Core;
using FluentAssertions;

using Moq;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Catalog.IntegrationTests
{
    public class ProductServiceTest
    {
        [Fact]
        public async Task GetAll_WhenExists_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    CategoryId = 1,
                    Amount = 5,
                    Description = "Desc1",
                    Price = 5m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    CategoryId = 2,
                    Amount = 7,
                    Description = "Desc2",
                    Price = 1m
                },
            };
            var categories = new List<Category>
            {
                new Category { Id = 1 },
                new Category { Id = 2 }
            };
            var fixture = GetFixture(categories, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetAll();

            // Assert
            result.Should().BeEquivalentTo(products);
        }

        [Fact]
        public async Task GetAll_WhenNothingExists_ShouldReturnEmptyCollection()
        {
            // Arrange
            var fixture = GetFixture();
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetAll();

            // Assert
            result.Should().BeEmpty();
        }

        [Fact]
        public async Task GetById_WhenExists_ShouldReturnItem()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    CategoryId = 1,
                    Amount = 5,
                    Description = "Desc1",
                    Price = 5m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    CategoryId = 2,
                    Amount = 7,
                    Description = "Desc2",
                    Price = 1m
                },
            };
            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetById(2);

            // Assert
            result.Should().BeEquivalentTo(products[1]);
        }

        [Fact]
        public async Task GetById_WhenCProductNotFound_ShouldrETURNnULL()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    CategoryId = 2,
                    Amount = 7,
                    Description = "Desc2",
                    Price = 1m
                },
            };
            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            Func<Task> act = async () => await service.GetById(1);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Product 1 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Fact]
        public async Task GetFilteredCount_WhenCategoryNotNullAndNotExists_ShouldThrowException()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    CategoryId = 1,
                    Amount = 5,
                    Description = "Desc1",
                    Price = 5m
                }
            };
            var categories = new List<Category>
            {
                new Category { Id = 1 },
            };
            var fixture = GetFixture(categories, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            Func<Task> act = async () => await service.GetFilteredCount(2);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task GetFilteredCount_WhenParameterNullable_ShouldReturnCount()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };
            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetFilteredCount(null);

            // Assert
            result.Should().Be(3);
        }

        [Fact]
        public async Task GetFilteredCount_WhenParameterNotNullable_ShouldReturnCount()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 1,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categories = new List<Category>
            {
                new Category { Id = 1 },
            };
            var fixture = GetFixture(categories, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetFilteredCount(1);

            // Assert
            result.Should().Be(2);
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenCategoryNotNullAndNotExists_ShouldThrowException()
        {
            // Arrange
            var categories = new List<Category>
            {
                new Category { Id = 1 },
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            Func<Task> act = async () => await service.GetAllByCategoryIdWithPagination(2, 1, 1);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenHonestAndLastPage_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(null, 1, 3);

            // Assert
            result.Should().BeEquivalentTo(new List<Product>
            {
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                }
            });
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenNotHonestAndNotLastPage_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(null, 2, 1);

            // Assert
            result.Should().BeEquivalentTo(new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                }
            });
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenNotHonestAndLastPage_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(null, 2, 2);

            // Assert
            result.Should().BeEquivalentTo(new List<Product>
            {
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                }
            });
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenThePageMoreThenExisting_ShouldReturnEmptyCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 9,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(null, 2, 3);

            // Assert
            result.Should().BeEmpty();
        }

        [Fact]
        public async Task GetAllByCategoryIdWithPagination_WhenCategorySpecified_ShouldReturnCollection()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 7,
                    Description = "Desc1",
                    Price = 15m
                },
                new Product
                {
                    Id = 2,
                    Name = "Product2",
                    ImageUrl = "Url2",
                    Amount = 4,
                    CategoryId = 2,
                    Description = "Desc2",
                    Price = 7m
                },
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 7,
                    Description = "Desc3",
                    Price = 5m
                },
            };

            var categories = new List<Category>
            {
                new Category { Id = 7 },
            };
            var fixture = GetFixture(categories, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.GetAllByCategoryIdWithPagination(7, 1, 2);

            // Assert
            result.Should().BeEquivalentTo(new List<Product>
            {
                new Product
                {
                    Id = 3,
                    Name = "Product3",
                    ImageUrl = "Url3",
                    Amount = 6,
                    CategoryId = 7,
                    Description = "Desc3",
                    Price = 5m
                }
            });
        }

        [Fact]
        public async Task Add_WhenCategoryInvalid_ShouldNotAdd()
        {
            // Arrange
            var product = new Product
            {
                Id = 1,
                Name = "Product1",
                ImageUrl = "Url1",
                Amount = 1,
                CategoryId = 2,
                Description = "Desc1",
                Price = 15m
            };
            var categories = new List<Category>
            {
                new Category { Id = 1 },
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            Func<Task> act = async () => await service.Add(product);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task Add_WhenCategoryExists_ShouldAdd()
        {
            // Arrange
            var product = new Product
            {
                Id = 1,
                Name = "Product1",
                ImageUrl = "Url1",
                Amount = 1,
                CategoryId = 2,
                Description = "Desc1",
                Price = 15m
            };
            var categories = new List<Category>
            {
                new Category { Id = 1 },
                new Category { Id = 2 },
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.Add(product);

            // Assert
            result.Should().BeEquivalentTo(product);
        }

        [Fact]
        public async Task Update_WhenCategoryInvalid_ShouldNotUpdate()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 1,
                    Description = "Desc1",
                    Price = 15m
                },
            };
            var product = new Product
            {
                Id = 2,
                Name = "Product1",
                ImageUrl = "Url1",
                Amount = 1,
                CategoryId = 2,
                Description = "Desc1",
                Price = 15m
            };
            var categories = new List<Category>
            {
                new Category { Id = 1 },
            };
            var fixture = GetFixture(categories, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            Func<Task> act = async () => await service.Update(product);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Category 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.LinkedInstanceNotExist);
        }

        [Fact]
        public async Task Update_WhenProductNotFound_ShouldNotUpdate()
        {
            // Arrange
            var product = new Product
            {
                Id = 2,
                Name = "Product1",
                ImageUrl = "Url1",
                Amount = 1,
                CategoryId = 1,
                Description = "Desc1",
                Price = 15m
            };
            var categories = new List<Category>
            {
                new Category { Id = 1 },
            };
            var fixture = GetFixture(categories);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            Func<Task> act = async () => await service.Update(product);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Product 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Fact]
        public async Task Update_WhenCategoryExists_ShouldAdd()
        {
            // Arrange
            var product = new Product
            {
                Id = 1,
                Name = "Product2",
                ImageUrl = "Url2",
                Amount = 10,
                CategoryId = 2,
                Description = "Desc2",
                Price = 9m
            };
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 7,
                    Description = "Desc1",
                    Price = 15m
                },
            };

            var categories = new List<Category>
            {
                new Category { Id = 1 },
                new Category { Id = 2 },
            };
            var fixture = GetFixture(categories, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            var result = await service.Update(product);

            // Assert
            result.Should().BeEquivalentTo(product);
        }

        [Fact]
        public async Task Delete_WhenProductNotFound_ShouldNotUpdate()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 7,
                    Description = "Desc1",
                    Price = 15m
                },
            };
            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            Func<Task> act = async () => await service.Delete(2);

            // Assert
            var exception = await Assert.ThrowsAsync<ServiceException>(act);
            exception.Message.Should().Be("Product 2 not found.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Fact]
        public async Task Delete_WhenExists_ShouldDelete()
        {
            // Arrange
            var products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Product1",
                    ImageUrl = "Url1",
                    Amount = 1,
                    CategoryId = 7,
                    Description = "Desc1",
                    Price = 15m
                },
            };
            var fixture = GetFixture(null, products);
            using var dbContext = fixture.Context;
            var productRepository = new Repository<Product>(dbContext);
            var categoryRepository = new Repository<Category>(dbContext);
            
            var service = new ProductService(productRepository, categoryRepository);

            // Act
            await service.Delete(1);
            var result = await service.GetAll();

            // Assert
            result.Should().BeEmpty();
        }

        private SqlLiteFixture GetFixture(List<Category> categories = null, List<Product> products = null)
        {
            return new SqlLiteFixture(categories, products);
        }
    }
}
