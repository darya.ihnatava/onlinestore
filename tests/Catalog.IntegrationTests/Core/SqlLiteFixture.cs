﻿using Catalog.DataAccess;
using Catalog.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Catalog.IntegrationTests.Core
{
    public class SqlLiteFixture : IDisposable
    {
        private readonly Lazy<Context> _context;

        public SqlLiteFixture(List<Category> categories = null, List<Product> products = null)
        {
            _context = new Lazy<Context>(SqlLiteInMemoryContext(categories, products));
        }

        public Context Context => _context.Value;

        private Context SqlLiteInMemoryContext(List<Category> categories = null, List<Product> products = null)
        {
            var options = new DbContextOptionsBuilder<Context>()
                .UseSqlite("DataSource=:memory:")
                .Options;

            var context = new Context(options);
            context.Database.OpenConnection();
            context.Database.EnsureCreated();
            context.Category.AddRange(categories ?? new List<Category>());
            context.Product.AddRange(products ?? new List<Product>());
            context.SaveChanges();
            return context;
        }

        public void Dispose()
        {
            Context?.Dispose();
        }
    }
}
