using Cart.BusinessLogic;
using Cart.DataAccess;
using Cart.Domain.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Cart.Domain.Exceptions;
using Models = Cart.Domain.Models;
using Cart.Domain.Dependencies;

namespace Cart.IntegrationTests
{
    public class RepositoryTest
    {
        private const string DatabaseConnectionString = "Filename=Test; Connection=Shared";
        private static readonly Guid CartGuid = Guid.NewGuid();
        private readonly Models.Cart Cart = new Models.Cart
        {
            Id = CartGuid,
            Items = new List<CartItem>
                    {
                        new CartItem
                        {
                            Id = 1,
                            Name = "CartItem1",
                            ImageUrl = "Url1",
                            Price = 16m,
                            Quantity = 6
                        },
                        new CartItem
                        {
                            Id = 2,
                            Name = "CartItem2",
                            ImageUrl = "Url2",
                            Price = 17.30m,
                            Quantity = 3
                        }
                    }
        };
        private static readonly Guid CartToAddGuid = Guid.NewGuid();
        private readonly Models.Cart CartToAddTo = new Models.Cart
        {
            Id = CartToAddGuid,
            Items = new List<CartItem>
                    {
                        new CartItem
                        {
                            Id = 1,
                            Name = "CartItem1",
                            ImageUrl = "Url1",
                            Price = 16m,
                            Quantity = 6
                        },
                    }
        };
        private static readonly Guid CartToRemoveFromGuid = Guid.NewGuid();
        private readonly Models.Cart CartToRemoveFrom = new Models.Cart
        {
            Id = CartToRemoveFromGuid,
            Items = new List<CartItem>
                    {
                        new CartItem
                        {
                            Id = 1,
                            Name = "CartItem1",
                            ImageUrl = "Url1",
                            Price = 16m,
                            Quantity = 6
                        },
                    }
        };

        [SetUp]
        public void Setup()
        {
            var settings = new Mock<ISettingsProvider>();
            settings.Setup(x => x.GetDatabaseConnectionString()).Returns(DatabaseConnectionString);
            var repository = new CartRepository(settings.Object);
            repository.Create(Cart);
            repository.Create(CartToAddTo);
            repository.Create(CartToRemoveFrom);
        }

        [TearDown]
        public void CleanUp()
        {
            var settings = new Mock<ISettingsProvider>();
            settings.Setup(x => x.GetDatabaseConnectionString()).Returns(DatabaseConnectionString);
            var repository = new CartRepository(settings.Object);
            repository.Remove(CartGuid);
            repository.Remove(CartToAddGuid);
            repository.Remove(CartToRemoveFromGuid);
        }

        private CartService GetService()
        {
            var settings = new Mock<ISettingsProvider>();
            settings.Setup(x => x.GetDatabaseConnectionString()).Returns(DatabaseConnectionString);
            var repository = new CartRepository(settings.Object);
            var service = new CartService(repository);
            return service;
        }

        [Test]
        public void GetById_WhenExists_ShouldReturnTheCart()
        {
            // Arrange
            var service = GetService();

            // Act
            var result = service.GetById(CartGuid);

            // Assert
            result.Should().BeEquivalentTo(Cart);
        }

        [Test]
        public void GetById_WhenNotExists_ShouldReturnNull()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var service = GetService();

            // Act
            TestDelegate act = () => service.GetById(guid);

            // Assert
            var exception = Assert.Throws<ServiceException>(act);
            exception.Message.Should().Be($"There are no cart {guid}.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Test]
        public void AddItem_WhenNoSuchCart_ShouldCreateNew()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var item = new CartItem
            {
                Id = 1,
                Name = "Cart",
                ImageUrl = "htpp://url.com",
                Price = 1m,
                Quantity = 5
            };
            var service = GetService();

            // Act
            var result = service.AddItem(guid, item);

            // Asserst
            result.Should().BeEquivalentTo(new Models.Cart
            {
                Id = guid,
                Items = new List<CartItem> { item }
            });
        }

        [Test]
        public void AddItem_WhenCartItemAlreadyExists_ShouldThrowExceptin()
        {
            // Arrange
            var item = new CartItem
            {
                Id = 1,
                Name = "CartItem1",
                ImageUrl = "Url1",
                Price = 16m,
                Quantity = 6
            };
            var service = GetService();

            // Act
            TestDelegate act = () => service.AddItem(CartGuid, item);

            // Assert
            var exception = Assert.Throws<ServiceException>(act);
            exception.Message.Should().Be($"The item {item.Id} already exists in cart {CartGuid}.");
            exception.StatusCode.Should().Be(ErrorType.CollectionItemAlreadyExists);
        }

        [Test]
        public void AddItem_WhenNoSuchItemExists_ShouldAdd()
        {
            // Arrange
            var item = new CartItem
            {
                Id = 2,
                Name = "CartItem2",
                ImageUrl = "Url2",
                Price = 9m,
                Quantity = 3
            };
            var service = GetService();

            // Act
            var result = service.AddItem(CartToAddGuid, item);

            // Asserst
            var resultCart = new Models.Cart
            {
                Id = CartToAddGuid,
                Items = new List<CartItem>
                    {
                        new CartItem
                        {
                            Id = 1,
                            Name = "CartItem1",
                            ImageUrl = "Url1",
                            Price = 16m,
                            Quantity = 6
                        },
                        new CartItem
                        {
                            Id = 2,
                            Name = "CartItem2",
                            ImageUrl = "Url2",
                            Price = 9m,
                            Quantity = 3
                        }
                    }
            };
            result.Should().BeEquivalentTo(resultCart);
        }

        [Test]
        public void RemoveItem_WhenNoSuchCart_ShouldReturnFalse()
        {
            // Arrange
            var guid = Guid.NewGuid();
            var service = GetService();

            // Act
            TestDelegate act = () => service.RemoveItem(guid, 1);

            // Assert
            var exception = Assert.Throws<ServiceException>(act);
            exception.Message.Should().Be($"There are no cart {guid}.");
            exception.StatusCode.Should().Be(ErrorType.InstanceNotExist);
        }

        [Test]
        public void RemoveItem_WhenItemNotExists_ShouldReturnFalse()
        {
            // Arrange
            var service = GetService();

            // Act
            TestDelegate act = () => service.RemoveItem(CartGuid, 6);

            // Assert
            var exception = Assert.Throws<ServiceException>(act);
            exception.Message.Should().Be($"There are no item 6 in cart {CartGuid}.");
            exception.StatusCode.Should().Be(ErrorType.CollectionItemNotExists);
        }

        [Test]
        public void RemoveItem_WhenItemExists_ShouldRemoveAndReturnTrue()
        {
            // Arrange
            var service = GetService();

            // Act
            var result = service.RemoveItem(CartToRemoveFromGuid, 1);

            // Assert
            var expectedCart = new Models.Cart
            {
                Id = CartToRemoveFromGuid
            };
            result.Should().BeEquivalentTo(expectedCart);
        }
    }
}