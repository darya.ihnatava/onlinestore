﻿using Serilog;
using System;
using System.Threading.Tasks;

namespace Kafka.Base.Core
{
    /// <summary>
    /// The kafka client base.
    /// </summary>
    public abstract class KafkaClientBase : IDisposable
    {
        private readonly KafkaSettingsBase _settings;

        protected KafkaClientBase(
            ILogger logger,
            KafkaSettingsBase settings)
        {
            _settings = settings;
            Logger = logger;
        }

        protected ILogger Logger { get; set; }

        protected async Task HandleInternalTask(Task operationalTask)
        {
            var timeoutTask = Task.Delay(_settings.Timeout);
            await Task.WhenAny(timeoutTask, operationalTask);

            if (timeoutTask.IsCompleted && !operationalTask.IsCompletedSuccessfully)
            {
                throw new KafkaTimeoutException("Having trouble with Kafka side. The time allotted for a process has expired.");
            }
        }

        /// <summary>
        /// Dispose object.
        /// </summary>
        public abstract void Dispose();
    }
}
