﻿namespace Kafka.Base.Core
{
    /// <summary>
    /// THe kafka base settings.
    /// </summary>
    public class KafkaSettingsBase
    {
        /// <summary>
        /// /// The bootstrap servers.
        /// /// </summary>
        public string BootstrapServers { get; set; }

        /// <summary>
        /// The topic.
        /// </summary>
        public string Topic { get; set; }

        /// <summary>
        /// The timeout.
        /// </summary>
        public int Timeout { get; set; }
    }
}
