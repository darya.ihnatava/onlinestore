﻿using Confluent.Kafka;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Kafka.Base.Handlers
{
    /// <summary>
    /// The consumer handlers
    /// </summary>
    /// <typeparam name="K">The key type.</typeparam>
    /// <typeparam name="V">The value type.</typeparam>
    [ExcludeFromCodeCoverage]
    public class ConsumerHandlers<K, V>
    {
        /// <summary>
        /// The error handler.
        /// </summary>
        public Action<IConsumer<K, V>, Error> ErrorHandler { get; set; }

        /// <summary>
        /// The log handler.
        /// </summary>
        public Action<IConsumer<K, V>, LogMessage> LogHandler { get; set; }
    }
}
