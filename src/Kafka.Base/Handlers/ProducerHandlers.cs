﻿using Confluent.Kafka;
using System;
using System.Diagnostics.CodeAnalysis;

namespace Kafka.Base.Handlers
{
    /// <summary>
    /// The producer handlers
    /// </summary>
    /// <typeparam name="K">The key type.</typeparam>
    /// <typeparam name="V">The value type.</typeparam>
    [ExcludeFromCodeCoverage]
    public class ProducerHandlers<K, V>
    {
        /// <summary>
        /// The error handler.
        /// </summary>
        public Action<IProducer<K, V>, Error> ErrorHandler { get; set; }

        /// <summary>
        /// The log handler.
        /// </summary>
        public Action<IProducer<K, V>, LogMessage> LogHandler { get; set; }
    }
}
