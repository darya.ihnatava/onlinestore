﻿using Confluent.Kafka;
using Kafka.Base.Handlers;
using Kafka.Base.JsonKafkaConverters;

namespace Kafka.Base
{
    /// <summary>
    /// The kafka client provider.
    /// </summary>
    /// <typeparam name="T">The key type.</typeparam>
    /// <typeparam name="V">The value type.</typeparam>
    public interface IKafkaClientProvider<K, V>
    {
        /// <summary>
        /// Creates producer.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="handlers">The handlers.</param>
        /// <returns>The producer.</returns>
        IProducer<K, V> CreateProducer(ProducerConfig config, ProducerHandlers<K, V> handlers);

        /// <summary>
        /// Created consumer.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="handlers">The handlers.</param>
        /// <returns>THe consumer.</returns>
        IConsumer<K, V> CreateConsumer(ConsumerConfig config, ConsumerHandlers<K, V> handlers);
    }

    /// <inheritdoc />
    public class KafkaClientProvider<K, V> : IKafkaClientProvider<K, V>
    {
        /// <inheritdoc />
        public IProducer<K, V> CreateProducer(ProducerConfig config, ProducerHandlers<K, V> handlers)
        {
            var producer = new ProducerBuilder<K, V>(config)
                .SetValueSerializer(new JsonKafkaSerializer<V>())
                .SetErrorHandler(handlers.ErrorHandler)
                .SetLogHandler(handlers.LogHandler)
                .Build();
            return producer;
        }

        /// <inheritdoc />
        public IConsumer<K, V> CreateConsumer(ConsumerConfig config, ConsumerHandlers<K, V> handlers)
        {
            var producer = new ConsumerBuilder<K, V>(config)
                .SetValueDeserializer(new JsonKafkaDeserializer<V>())
                .SetErrorHandler(handlers.ErrorHandler)
                .SetLogHandler(handlers.LogHandler)
                .Build();
            return producer;
        }
    }
}
