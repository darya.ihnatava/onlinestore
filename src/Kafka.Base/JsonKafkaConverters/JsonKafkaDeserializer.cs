﻿using Confluent.Kafka;
using System;
using System.Text;
using System.Text.Json;

namespace Kafka.Base.JsonKafkaConverters
{
    /// <summary>
    /// The json kafka serilizer.
    /// </summary>
    /// <typeparam name="T">The type</typeparam>
    internal class JsonKafkaDeserializer<T> : IDeserializer<T>
    {
        /// <summary>
        /// Deserilizes data from kafka.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="isNull">Is nllable.</param>
        /// <param name="context">The context.</param>
        /// <returns>Deserilized entity.</returns>
        public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
        {
            var json = Encoding.UTF8.GetString(data);
            return JsonSerializer.Deserialize<T>(json);
        }
    }
}