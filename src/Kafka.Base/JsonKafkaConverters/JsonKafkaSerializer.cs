﻿using Confluent.Kafka;
using System.Text;
using System.Text.Json;

namespace Kafka.Base.JsonKafkaConverters
{
    /// <summary>
    /// The json kafka serilizer.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class JsonKafkaSerializer<T> : ISerializer<T>
    {
        /// <summary>
        /// Serializes the type.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="context"></param>
        /// <returns>Byte array of the serialized values.</returns>
        public byte[] Serialize(T data, SerializationContext context)
        {
            return Encoding.UTF8.GetBytes(JsonSerializer.Serialize(data));
        }
    }
}