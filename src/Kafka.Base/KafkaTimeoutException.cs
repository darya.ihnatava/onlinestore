﻿
using System;
using System.Runtime.Serialization;

namespace Kafka.Base
{
    public class KafkaTimeoutException : TimeoutException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KafkaTimeoutException"/> class.
        /// </summary>
        public KafkaTimeoutException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KafkaTimeoutException"/> class.
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="context">Context.</param>
        protected KafkaTimeoutException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KafkaTimeoutException"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        public KafkaTimeoutException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="KafkaTimeoutException"/> class.
        /// </summary>
        /// <param name="message">Message.</param>
        /// <param name="innerException">Exception.</param>
        public KafkaTimeoutException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}