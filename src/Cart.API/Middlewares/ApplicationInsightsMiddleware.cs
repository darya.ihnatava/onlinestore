﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace API.Shared.Core
{
    /// <summary>
    /// The application insights middleware.
    /// </summary>
    public class ApplicationInsightsMiddleware
    {
        private const string CorrelationIdHeader = "x-correlation-id";
        private readonly TelemetryClient telemetryClient = new TelemetryClient(TelemetryConfiguration.CreateDefault());
        private readonly RequestDelegate _next;

        /// <summary>
        /// Initializes a new instance of the <see cref="CorrelationIdMiddleware"/> class.
        /// </summary>
        public ApplicationInsightsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Invokes the middleware.
        /// </summary>
        public async Task Invoke(HttpContext context)
        {
            var requestTelemetry = new RequestTelemetry
            {
                Name = $"{context.Request.Method} {context.Request.Path}"
            };

            context.Request.Headers.TryGetValue(CorrelationIdHeader, out var correlationId);
            requestTelemetry.Context.Operation.Id = correlationId; 

            var operation = telemetryClient.StartOperation(requestTelemetry);

            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                requestTelemetry.Success = false;
                telemetryClient.TrackException(ex);
                throw;
            }
            finally
            {
                requestTelemetry.ResponseCode = context.Response.StatusCode.ToString();
                requestTelemetry.Success = true;
                telemetryClient.StopOperation(operation);
            }
        }
    }
}
