using API.Shared.Core;
using API.Shared.Swagger;
using AutoMapper;
using Cart.API.Core;
using Cart.API.Mappers;
using Cart.BusinessLogic;
using Cart.DataAccess;
using Cart.Domain.Dependencies;
using Kafka.Base;
using Kafka.Consumer;

using Microsoft.ApplicationInsights.DependencyCollector;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using System;

namespace Cart.API
{
    /// <summary>
    /// The main configuration of the api.
    /// </summary>
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private const string ApiDescription = "CArt API is a system that provides cart information.";
        private const string ApiTitle = "Cart API";
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            InitializeLogger(services);
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            services.AddSingleton<IMapper>(mapperConfig.CreateMapper());
            services.Configure<Settings>(_configuration);
            services.AddSingleton<ISettingsProvider, SettingsProvider>();
            services.AddServiceDependencies();
            services.AddRepositoryDependencies();

            services.AddControllers();
            services.AddSwaggerConfiguration(ApiTitle, ApiDescription);
            services.AddVersionedApiExplorer(opt => opt.GroupNameFormat = "'v'VVV");
            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
                config.ApiVersionReader = ApiVersionReader.Combine(new HeaderApiVersionReader("x-version"), new UrlSegmentApiVersionReader());
            });

            services.AddSingleton(typeof(IKafkaClientProvider<,>), typeof(KafkaClientProvider<,>));
            services.Configure<KafkaSettings>(_configuration.GetSection("Kafka"));
            services.AddSingleton<IKafkaConsumer, KafkaConsumer>();
            services.AddSingleton<ConsumerService>();
            services.AddHostedService<ConsumerService>(); 
            
            services.AddMvcCore(options => options.Filters.Add<ApiExceptionFilterAttribute>())
                    .AddApiExplorer()
                    .AddDataAnnotations();
            services.AddApplicationInsightsTelemetry(Configuration["APPINSIGHTS_CONNECTIONSTRING"]);
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="apiVersionDescriptionProvider">The API version description provider.</param>
        /// <param name="serviceProvider">The service provider.</param>
        public void Configure(
            IApplicationBuilder app,
            IApiVersionDescriptionProvider apiVersionDescriptionProvider,
            IServiceProvider serviceProvider)
        {
            var loggerFactory = serviceProvider.GetService<ILoggerFactory>();
            loggerFactory.AddSerilog();

            app.UseDeveloperExceptionPage();

            app.UseConfiguredSwaggerInApp(apiVersionDescriptionProvider);
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseMiddleware<CorrelationIdMiddleware>();
            app.UseMiddleware<ApplicationInsightsMiddleware>();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void InitializeLogger(IServiceCollection services)
        {
            if (Log.Logger == null)
            {
                const string exceptionMessage = "Logger instance was not initialized before registering into DI system";
                throw new InvalidOperationException(exceptionMessage);
            }

            Log.Logger = new LoggerConfiguration()
                    .ReadFrom
                    .Configuration(_configuration, "Serilog")
                    .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Warning)
                    .CreateLogger();
            services.AddSingleton(Log.Logger);
        }
    }
}
