﻿using System;
using System.Collections.Generic;

namespace Cart.API.Models
{
    /// <summary>
    /// THe cart response.
    /// </summary>
    public class CartResponse
    {
        /// <summary>
        /// The cart unique identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The cart items.
        /// </summary>
        public List<CartItemResponse> Items { get; set; } = new List<CartItemResponse>();
    }
}
