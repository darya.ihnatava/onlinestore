﻿namespace Cart.API.Models
{
    /// <summary>
    /// THe cart response.
    /// </summary>
    public class CartItemResponse
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// The quantity.
        /// </summary>
        public int Quantity { get; set; }
    }
}
