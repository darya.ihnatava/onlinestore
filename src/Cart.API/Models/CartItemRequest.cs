﻿using API.Shared.Core;
using System.ComponentModel.DataAnnotations;

namespace Cart.API.Models
{
    /// <summary>
    /// The cart item request.
    /// </summary>
    public class CartItemRequest
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        [CustomValidation(typeof(ModelValidator), nameof(ModelValidator.GreaterThenZero))]
        public int Id { get; set; }

        /// <summary>
        /// The name.
        /// </summary>
        [MinLength(5)]
        [MaxLength(150)]
        public string Name { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        [Url]
        public string ImageUrl { get; set; }

        /// <summary>
        /// The price.
        /// </summary>
        [CustomValidation(typeof(ModelValidator), nameof(ModelValidator.GreaterThenZero))]
        public decimal Price { get; set; }

        /// <summary>
        /// The quantity.
        /// </summary>
        [CustomValidation(typeof(ModelValidator), nameof(ModelValidator.GreaterThenZero))]
        public int Quantity { get; set; }
    }
}
