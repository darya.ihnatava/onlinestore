﻿using Cart.Domain.Dependencies;
using Microsoft.Extensions.Options;

namespace Cart.API.Core
{
    /// <summary>
    /// The application settings.
    /// </summary>
    public class SettingsProvider : ISettingsProvider
    {
        private readonly IOptions<Settings> _options;

        /// <summary>
        /// The settings provider.
        /// </summary>
        /// <param name="options">The options.</param>
        public SettingsProvider(IOptions<Settings> options)
        {
            _options = options;
        }

        /// <inheritdoc />
        public string GetDatabaseConnectionString()
        {
            return _options.Value.ConnectionString;
        }
    }
}
