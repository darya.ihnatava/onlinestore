﻿namespace Cart.API.Core
{
    /// <summary>
    /// The application settings.
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// The connection string.
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
