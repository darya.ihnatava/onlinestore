﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading.Tasks;
using Cart.API.ErrorHandling;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Cart.API.Core
{
    /// <summary>
    /// Extensions for working directly with <see cref="HttpContext"/>.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class HttpContextExtensions
    {
        /// <summary>
        /// Json format files.
        /// </summary>
        public const string JsonSerializationType = "application/json";

        /// <summary>
        /// Write an <see cref="ApiError"/>.
        /// </summary>
        public static Task WriteErrorAsync(this HttpContext context, ApiError error, HttpStatusCode responseCode = HttpStatusCode.BadRequest)
        {
            var errorJson = JsonConvert.SerializeObject(error);
            context.Response.StatusCode = (int)responseCode;
            context.Response.ContentType = JsonSerializationType;
            return context.Response.WriteAsync(errorJson);
        }
    }
}
