﻿using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;
using Cart.Domain.Exceptions;

namespace Cart.API.ErrorHandling
{
    /// <summary>
    /// Class for storing only general information about exceptions.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class ApiError
    {
        /// <summary>
        /// The System.Exception.
        /// </summary>
        private readonly Exception _exception;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiError"/> class. The error does use a specific description.
        /// </summary>
        /// <param name="errorData">Error data.</param>
        [JsonConstructor]
        public ApiError(Error errorData)
        {
            Error = errorData;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiError"/> class. The error does not have a specific description.
        /// </summary>
        /// <param name="exception">The incoming exception.</param>
        /// <param name="code">Status code.</param>
        public ApiError(Exception exception, ErrorType code)
            : this(exception.Message, exception, code)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiError"/> class.
        /// </summary>
        /// <param name="description">Overall description of error explaining which part of system in general is broken.</param>
        /// <param name="exception">The exception that caused the error.</param>
        /// <param name="code">Status code.</param>
        public ApiError(string description, Exception exception = null, ErrorType code = ErrorType.Unknown)
        {
            _exception = exception;
            Error = GenerateErrorData(description, code);
        }

        /// <summary>
        /// Overall description of error explaining which part of system in general is broken.
        /// </summary>
        [JsonProperty(PropertyName = "error")]
        public Error Error { get; set; }

        /// <summary>
        /// Returns a string representation of ApiError.
        /// </summary>
        /// <returns>The <see cref="string"/>.</returns>
        public override string ToString()
        {
            return $"{Error.Code}-{Error.Message}";
        }

        private Error GenerateErrorData(string description, ErrorType code)
        {
            if (_exception == null)
            {
                return new Error
                {
                    Code = code,
                    Message = description,
                };
            }

            return new Error
            {
                Code = code,
                Message = _exception.Message,
                Target = _exception.Source,
                InnerError = _exception.InnerException != null ? ToInnerError(_exception.InnerException) : null,
            };
        }

        private Error ToInnerError(Exception innerException)
        {
            var statusCode = _exception.GetType().GetProperty("StatusCode");
            return new Error
            {
                Code = statusCode != null ? (ErrorType)statusCode.GetValue(_exception) : ErrorType.Unknown,
                Message = innerException.Message,
                Target = innerException.Source,
                InnerError = innerException.InnerException != null ? ToInnerError(innerException.InnerException) : null,
            };
        }
    }
}
