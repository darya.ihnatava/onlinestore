﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Diagnostics.CodeAnalysis;
using Cart.Domain.Exceptions;

namespace Cart.API.ErrorHandling
{
    /// <summary>
    /// The error object.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Error
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Error"/> class.
        /// </summary>s
        [JsonConstructor]
        public Error()
        {
        }

        /// <summary>
        /// One of a server-defined set of error codes.
        /// </summary>
        [JsonProperty(ItemConverterType = typeof(StringEnumConverter), PropertyName = "code")]
        public ErrorType Code { get; set; }

        /// <summary>
        /// A human-readable representation of the error.
        /// </summary>
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        /// <summary>
        /// The target of the error.
        /// </summary>
        [JsonProperty(PropertyName = "target", NullValueHandling = NullValueHandling.Ignore)]
        public string Target { get; set; }

        /// <summary>
        /// An object containing more specific information than the current object about the error.
        /// </summary>
        [JsonProperty(PropertyName = "innererror", NullValueHandling = NullValueHandling.Ignore)]
        public Error InnerError { get; set; }
    }
}
