﻿using AutoMapper;
using Cart.API.ErrorHandling;
using Cart.API.Models;
using Cart.BusinessLogic;
using Cart.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Cart.API.Controllers
{
    /// <summary>
    /// The cart controller.
    /// </summary>
    [ApiController]
    [Route("api/v{version:apiVersion}/carts")]
    [ApiVersion("1.0", Deprecated = true)]
    [ApiVersion("2.0")]
    public class CartController : Controller
    {
        private readonly ICartService _cartService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes Cart Controller instance.
        /// </summary>
        /// <param name="cartService">The Cart Service.</param>
        /// <param name="mapper">The mapper.></param>
        public CartController(
            ICartService cartService,
            IMapper mapper)
        {
            _mapper = mapper;
            _cartService = cartService;
        }

        /// <summary>
        /// Gets the cart by identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        /// <response code="200">The cart data successfully find and returned.</response>
        /// <response code="404">The cart not exists.</response>
        /// <returns>The cart.</returns>
        [HttpGet]
        [Route("{cartId}")]
        [ApiVersion("1.0", Deprecated = true)]
        [ProducesResponseType(typeof(List<CartResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public IActionResult GetById(Guid cartId)
        {
            var cart = _cartService.GetById(cartId);
            var responseCart = _mapper.Map<CartResponse>(cart);
            return Ok(responseCart);
        }

        /// <summary>
        /// Gets the cart by identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        /// <response code="200">The cart successfully find and items of cart returned.</response>
        /// <response code="404">The cart not exists.</response>
        /// <returns>The cart.</returns>
        [HttpGet]
        [Route("{cartId}")]
        [ApiVersion("2.0")]
        [ProducesResponseType(typeof(List<List<CartResponse>>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public IActionResult GetCartItemsByCartId(Guid cartId)
        {
            var cart = _cartService.GetById(cartId);
            var responseCartItems = _mapper.Map<List<CartItemResponse>>(cart.Items);
            return Ok(responseCartItems);
        }

        /// <summary>
        /// Adds new item to cart by identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        /// <param name="request">The request with cart item to add data.</param>
        /// <response code="200">The cart created.</response>
        /// <response code="200">The bad request. Possible item already exists.</response>
        /// <returns>The updated cart.</returns>
        [HttpPost]
        [Route("{cartId}/items")]
        [ProducesResponseType(typeof(List<CartResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        public IActionResult AddToCartById(Guid cartId, [FromBody] CartItemRequest request)
        {
            var item = _mapper.Map<CartItem>(request);
            var cart = _cartService.AddItem(cartId, item);
            var responseCart = _mapper.Map<CartResponse>(cart);
            return Ok(responseCart);
        }

        /// <summary>
        /// Removes the item from cart by identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        /// <param name="cartItemId">The identifier of cart item to delete.</param>
        /// <response code="200">The item removed from cart.</response>
        /// <response code="404">The cart not found.</response>
        /// <response code="404">The bad request. Possibly the wrong item to remove.</response>
        /// <returns>The updated cart.</returns>
        [HttpDelete]
        [Route("{cartId}/items/{cartItemId}")]
        [ProducesResponseType(typeof(List<CartResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        public IActionResult RemoveFromCartById(Guid cartId, int cartItemId)
        {
            var cart = _cartService.RemoveItem(cartId, cartItemId);
            var responseCart = _mapper.Map<CartResponse>(cart);
            return Ok(responseCart);
        }
    }
}
