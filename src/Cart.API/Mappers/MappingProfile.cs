﻿using AutoMapper;
using Cart.API.Models;
using Cart.Domain.Models;

namespace Cart.API.Mappers
{
    /// <summary>
    /// The mapping profile.
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes mapping entities.
        /// </summary>
        public MappingProfile()
        {
            CreateMap<CartItemRequest, CartItem>();
            CreateMap<Domain.Models.Cart, CartResponse>();
            CreateMap<CartItem, CartItemResponse>();
        }
    }
}
