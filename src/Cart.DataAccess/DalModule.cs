﻿using Cart.Domain.Dependencies;
using Microsoft.Extensions.DependencyInjection;

namespace Cart.DataAccess
{
    /// <summary>
    /// The registration on business logic services.
    /// </summary>
    public static class DalModule
    {
        /// <summary>
        /// The method on business logic services.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void AddRepositoryDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICartRepository, CartRepository>();
        }
    }
}
