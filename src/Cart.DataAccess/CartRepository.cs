﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cart.Domain.Dependencies;
using LiteDB;

namespace Cart.DataAccess
{
    /// <inheritdoc />
    public class CartRepository : ICartRepository
    {
        private const string CollectionName = "Cart";
        private const string ItemsIndexKey = "index_items";
        private const string ItemsIndexValue = "$.Items[*].Id";
        private readonly ISettingsProvider _settingsProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="ICartRepository"/> class.
        /// </summary>
        /// <param name="settingsProvider">The settings provider.</param>
        public CartRepository(
            ISettingsProvider settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        /// <inheritdoc />
        public Domain.Models.Cart GetById(Guid cartId)
        {
            using var database = new LiteDatabase(_settingsProvider.GetDatabaseConnectionString());
            var cart = database.GetCollection<Domain.Models.Cart>(CollectionName).FindById(cartId);
            return cart;
        }

        /// <inheritdoc />
        public Domain.Models.Cart Create(Domain.Models.Cart cart)
        {
            using var database = new LiteDatabase(_settingsProvider.GetDatabaseConnectionString());
            var carts = database.GetCollection<Domain.Models.Cart>(CollectionName);
            carts.Insert(cart);

            return cart;
        }

        /// <inheritdoc />
        public void Remove(Guid cartId)
        {
            using var database = new LiteDatabase(_settingsProvider.GetDatabaseConnectionString());
            var carts = database.GetCollection<Domain.Models.Cart>(CollectionName);
            carts.Delete(cartId);
        }

        /// <inheritdoc />
        public Domain.Models.Cart Update(Domain.Models.Cart cart)
        {
            using var database = new LiteDatabase(_settingsProvider.GetDatabaseConnectionString());
            var carts = database.GetCollection<Domain.Models.Cart>(CollectionName);
            carts.Update(cart);
            return cart;
        }

        /// <inheritdoc />
        public List<Domain.Models.Cart> GetCartsByProductId(int productId)
        {
            using var database = new LiteDatabase(_settingsProvider.GetDatabaseConnectionString());
            var dbCarts = database.GetCollection<Domain.Models.Cart>(CollectionName);
            dbCarts.EnsureIndex(ItemsIndexKey, ItemsIndexValue);
            var carts = dbCarts.Find(x => x.Items[0].Id == productId).ToList();
            return carts.ToList();
        }
    }
}
