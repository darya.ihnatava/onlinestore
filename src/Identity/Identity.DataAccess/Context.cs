﻿using Identity.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Identity.DataAccess
{
    /// <summary>
    /// The context.
    /// </summary>
    /// <seealso cref="DbContext" />
    public class Context : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Context"/> class.
        /// </summary>
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// The users.
        /// </summary>
        public DbSet<User> User { get; set; }

        /// <summary>
        /// The roles..
        /// </summary>
        public DbSet<Role> Role { get; set; }

        /// <summary>
        /// The permissions..
        /// </summary>
        public DbSet<Permission> Permission { get; set; }


        /// <summary>
        /// On model creating method.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey(x => new { x.Id });
            modelBuilder.Entity<Role>().HasKey(x => new { x.Id });
            modelBuilder.Entity<Permission>().HasKey(x => new { x.Id });
        }
    }
}
