﻿using Identity.Domain;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Identity.DataAccess
{
    /// <summary>
    /// The registration on business logic services.
    /// </summary>
    public static class DAModule
    {
        /// <summary>
        /// The method on business logic services.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void AddRepositoryDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient(typeof(IRepository<>), typeof(Repository<>));

            var connectionString = serviceCollection.BuildServiceProvider()
                                           .GetService<ISettingsProvider>().GetDatabaseConnectionString();
            serviceCollection.AddDbContext<Context>(options => options.UseSqlServer(connectionString));
        }
    }
}
