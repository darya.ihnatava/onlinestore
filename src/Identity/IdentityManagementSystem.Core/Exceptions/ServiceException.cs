﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace Identity.BusinessLogic.Exceptions
{
    /// <summary>
    /// An exception that will be handled gracefully at the top level.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [Serializable]
    public class ServiceException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceException"/> class.
        /// </summary>
        public ServiceException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        public ServiceException(string message)
            : base(message)
        {
            Description = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="statusCode">The HTTP status code.</param>
        public ServiceException(string message, ErrorType statusCode)
            : base(message)
        {
            Description = message;
            StatusCode = statusCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceException"/> class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="innerException">The inner Exception.</param>
        public ServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
            Description = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceException"/> class.
        /// </summary>
        /// <param name="info">The Serialization info.</param>
        /// <param name="context">The Streaming context.</param>
        protected ServiceException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// A user-visible description of the error.
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// A status code of the response.
        /// </summary>
        public ErrorType StatusCode { get; }
    }
}