﻿namespace Identity.BusinessLogic.Exceptions
{
    /// <summary>
    /// The error type.
    /// </summary>
    public enum ErrorType
    {
        /// <summary>
        /// Unknown error.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The user not found.
        /// </summary>
        UserNotExist = 1,

        /// <summary>
        /// The name already exists;
        /// </summary>
        DuplicateName = 2,

        /// <summary>
        /// Wrong password or user name.
        /// </summary>
        WrongNameOrPassword = 3,

        /// <summary>
        /// THe bad request.
        /// </summary>
        BadRequest = 4,
    }
}
