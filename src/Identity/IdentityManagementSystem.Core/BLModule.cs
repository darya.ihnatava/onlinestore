﻿using Identity.BusinessLogic.Cryptography;
using Identity.BusinessLogic.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Identity.BusinessLogic
{
    /// <summary>
    /// The registration on business logic services.
    /// </summary>
    public static class BLModule
    {
        /// <summary>
        /// The method on business logic services.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void AddServiceDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IUserService, UserService>();
            serviceCollection.AddTransient<IRoleService, RoleService>();
            serviceCollection.AddTransient<IHashService, HashService>();
            serviceCollection.AddTransient<ITokenService, TokenService>();
        }
    }
}
