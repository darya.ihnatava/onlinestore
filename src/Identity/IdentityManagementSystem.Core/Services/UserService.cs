﻿using Identity.BusinessLogic.Cryptography;
using Identity.BusinessLogic.Exceptions;
using Identity.BusinessLogic.Models;
using Identity.Domain;
using Identity.Domain.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.BusinessLogic.Services
{
    /// <summary>
    /// The user service.
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Register user.
        /// </summary>
        /// <param name="user">The user.</param>
        Task Register(User user);

        /// <summary>
        /// Login user.
        /// </summary>
        /// <param name="user">The user login information.</param>
        /// <returns>The user role model.</returns>
        UserRoleModel Login(User user);
    }

    /// <inheritdoc />
    public class UserService : IUserService
    {
        private readonly IHashService _hashService;
        private readonly IRoleService _roleService;
        private readonly IRepository<User> _userRepository;

        public UserService(
            IHashService hashService,
            IRoleService roleService,
            IRepository<User> userRepository)
        {
            _hashService = hashService;
            _roleService = roleService;
            _userRepository = userRepository;
        }

        /// <inheritdoc />
        public async Task Register(User user)
        {
            EnsureNameNotExists(user.Name);
            EnsureRoleExistence(user.RoleId);

            var hash = _hashService.HashPassword(user.Password);
            user.Password = hash;

            await _userRepository.Add(user);
        }

        /// <inheritdoc />
        public UserRoleModel Login(User user)
        {
            var existsUser = EnsureUserExists(user.Name);
            var isValid = _hashService.VerifyPassword(user.Password, existsUser.Password);
            if (!isValid)
            {
                throw new ServiceException($"Wrong username or password.", ErrorType.WrongNameOrPassword);
            }

            var permissions = _roleService.GetById(existsUser.RoleId).Permissions;
            var userRoleModel = new UserRoleModel
            {
                Id = existsUser.Id,
                Name = existsUser.Name,
                Permissions = permissions.Select(x => x.Path).ToList()
            };

            return userRoleModel;
        }

        private void EnsureNameNotExists(string name)
        {
            var isAlreadyExists = _userRepository.GetAll().Any(x => x.Name == name);
            if (isAlreadyExists)
            {
                throw new ServiceException($"The name {name} already exists.", ErrorType.DuplicateName);
            }
        }

        private User EnsureUserExists(string name)
        {
            var user = _userRepository.GetAll().FirstOrDefault(x => x.Name == name);
            if (user == null)
            {
                throw new ServiceException($"The user {name} does not exists.", ErrorType.UserNotExist);
            }

            return user;
        }

        private void EnsureRoleExistence(int roleId)
        {
            _roleService.GetById(roleId);
        }
    }
}
