﻿using Identity.BusinessLogic.Models;
using Identity.Domain;
using Identity.Domain.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Identity.BusinessLogic.Services
{
    /// <summary>
    /// The token service.
    /// </summary>
    public interface ITokenService
    {
        /// <summary>
        /// Creates token.
        /// </summary>
        /// <param name="userRoleModel">The user role model.</param>
        /// <returns>The token.</returns>
        string CreateToken(UserRoleModel userRoleModel);

        /// <summary>
        /// Validates the token.
        /// </summary>
        /// <param name="token">THe token.</param>
        /// <returns>The result of validation.</returns>
        bool IsTokenValid(string token, string path);
    }

    public class TokenService : ITokenService
    {
        private readonly AuthSettings _settings;

        public TokenService(
            ISettingsProvider settingsProvider)
        {
            _settings = settingsProvider.GetAuthSettings();
        }

        /// <inheritdoc />
        public string CreateToken(UserRoleModel userRoleModel)
        {
            var claims = new List<Claim>
            {
                new (nameof(UserRoleModel.Id), userRoleModel.Id.ToString()),
                new (nameof(UserRoleModel.Name), userRoleModel.Name)
            };
            userRoleModel.Permissions.ForEach(x => claims.Add(new Claim(nameof(Permission), x)));

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var tokenDescriptor = new JwtSecurityToken(
                _settings.Key,
                _settings.Issuer,
                claims,
                expires: DateTime.Now.AddMinutes(_settings.TokenExpirationInMinutes),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);
        }

        /// <inheritdoc />
        public bool IsTokenValid(string token, string path)
        {
            var mySecret = Encoding.UTF8.GetBytes(_settings.Key);
            var mySecurityKey = new SymmetricSecurityKey(mySecret);
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                var principal = tokenHandler.ValidateToken(token,
                new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    IssuerSigningKey = mySecurityKey,
                }, out SecurityToken validatedToken);
                if (path == ":")
                {
                    return true;
                }

                var permissions = principal.FindAll("Permission").Select(x => x.Value).ToList();
                return permissions.Any(x => x == path);
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
