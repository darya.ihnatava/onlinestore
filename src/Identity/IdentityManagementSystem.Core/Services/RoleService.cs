﻿using Identity.BusinessLogic.Exceptions;
using Identity.BusinessLogic.Models;
using Identity.Domain;
using Identity.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.BusinessLogic.Services
{
    public interface IRoleService
    {
        /// <summary>
        /// Gets role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <returns>The role model.</returns>
        RoleModel GetById(int roleId);

        /// <summary>
        /// Adds role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>The role model.</returns>
        Task<RoleModel> Add(RoleModel role);

        /// <summary>
        /// Updates role.
        /// </summary>
        /// <param name="role">The role model.</param>
        /// <returns>The role model.</returns>
        Task<RoleModel> Update(RoleModel role);

        /// <summary>
        /// Deletes the role.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        Task Delete(int roleId);
    }

    /// <inheritdoc />
    public class RoleService : IRoleService
    {
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Permission> _permissionRepository;

        public RoleService(
            IRepository<Role> roleRepository,
            IRepository<Permission> permissionRepository)
        {
            _roleRepository = roleRepository;
            _permissionRepository = permissionRepository;
        }

        /// <inheritdoc />
        public RoleModel GetById(int roleId)
        {
            EnsureExistence(roleId);
            var query = (from role in _roleRepository.GetAll()
                         join permission in _permissionRepository.GetAll() on role.Id equals permission.RoleId
                         where role.Id == roleId
                         select new { role, permission }).ToList();
            var list = from item in query
                       group item by new { item.role.Id, item.role.Name } into gr
                       select new RoleModel
                       {
                           Id = gr.Key.Id,
                           Name = gr.Key.Name,
                           Permissions = gr.Select(x => x.permission).ToList()
                       };
            var result = list.FirstOrDefault();
            return result;
        }

        /// <inheritdoc />
        public async Task Delete(int roleId)
        {
            EnsureExistence(roleId);
            await _roleRepository.Delete(roleId);
        }

        /// <inheritdoc />
        public async Task<RoleModel> Add(RoleModel role)
        {
            EnsureNotExist(role.Name);
            var roleEntity = new Role
            {
                Id = role.Id,
                Name = role.Name
            };
            await _roleRepository.Add(roleEntity);
            await AddRolePermissions(role);
            return new RoleModel
            {
                Id = roleEntity.Id,
                Name = roleEntity.Name,
                Permissions = role.Permissions
            };
        }

        /// <inheritdoc />
        public async Task<RoleModel> Update(RoleModel role)
        {
            EnsureExistence(role.Id);
            var roleEntity = new Role
            {
                Id = role.Id,
                Name = role.Name
            };
            await _roleRepository.Update(roleEntity);
            var existsPermissions = _permissionRepository.GetAll().Where(x => x.RoleId == role.Id).ToList();
            await _permissionRepository.DeleteRange(existsPermissions);
            await AddRolePermissions(role);
            return new RoleModel
            {
                Id = roleEntity.Id,
                Name = roleEntity.Name,
                Permissions = role.Permissions
            };
        }

        private Role EnsureExistence(int roleId)
        {
            var role = _roleRepository.GetAll().FirstOrDefault(x => x.Id == roleId);
            if (role == null)
            {
                throw new ServiceException($"The role {roleId} does not exists.", ErrorType.UserNotExist);
            }

            return role;
        }

        private void EnsureNotExist(string roleName)
        {
            var role = _roleRepository.GetAll().FirstOrDefault(x => x.Name == roleName);
            if (role != null)
            {
                throw new ServiceException($"The role {role} already exists.", ErrorType.DuplicateName);
            }
        }

        private async Task AddRolePermissions(RoleModel role)
        {
            role.Permissions.ForEach(x => x.RoleId = role.Id);
            await _permissionRepository.AddRange(role.Permissions);
        }
    }
}
