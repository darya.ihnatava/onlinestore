﻿using Identity.Domain.Models;
using System.Collections.Generic;

namespace Identity.BusinessLogic.Models
{
    /// <summary>
    /// The role model.
    /// </summary>
    public class RoleModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The role name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The permissions collection.
        /// </summary>
        public List<Permission> Permissions { get; set; }
    }
}
