﻿using System.Collections.Generic;

namespace Identity.BusinessLogic.Models
{
    /// <summary>
    /// The user role model.
    /// </summary>
    public class UserRoleModel
    {
        /// <summary>
        /// THe identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// THe name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// THe user permissions.
        /// </summary>
        public List<string> Permissions { get; set; }
    }
}
