﻿namespace Identity.BusinessLogic.Cryptography
{
    /// <summary>
    /// The hashing service.
    /// </summary>
    public interface IHashService
    {
        /// <summary>
        /// Hasing password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns>The hashed password.</returns>
        string HashPassword(string password);

        /// <summary>
        /// Verify is hash the same as specified password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="hash">The hash.</param>
        /// <returns>The result of verification.</returns>
        bool VerifyPassword(string password, string hash);
    }

    /// <inheritdoc />
    public class HashService : IHashService
    {
        /// <inheritdoc />
        public string HashPassword(string password)
        {
            string passwordHash = BCrypt.Net.BCrypt.HashPassword(password);
            return passwordHash;
        }

        /// <inheritdoc />
        public bool VerifyPassword(string password, string hash)
        {
            bool isValid = BCrypt.Net.BCrypt.Verify(password, hash);
            return isValid;
        }
    }
}
