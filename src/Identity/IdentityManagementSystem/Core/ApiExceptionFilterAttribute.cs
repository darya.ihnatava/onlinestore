﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using Identity.BusinessLogic.Exceptions;
using IdentityManagementSystem.ErrorHandling;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;
using ExceptionFilterAttribute = Microsoft.AspNetCore.Mvc.Filters.ExceptionFilterAttribute;

namespace IdentityManagementSystem.Core
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    public sealed class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiExceptionFilterAttribute"/> class.
        /// </summary>
        public ApiExceptionFilterAttribute(ILogger logger)
        {
            _logger = logger;
        }

        /// <inheritdoc />
        public override void OnException(ExceptionContext context)
        {
            ApiError error;
            bool isValidationException = false;
            if (context.Exception is ServiceException ex)
            {
                isValidationException = true;
                HttpStatusCode httpCode;
                switch (ex.StatusCode)
                {
                    case ErrorType.UserNotExist:
                        httpCode = HttpStatusCode.NotFound;
                        break;
                    case ErrorType.DuplicateName:
                    case ErrorType.WrongNameOrPassword:
                        httpCode = HttpStatusCode.BadRequest;
                        break;
                    default:
                        httpCode = HttpStatusCode.InternalServerError;
                        isValidationException = false;
                        break;
                }

                context.HttpContext.Response.StatusCode = (int)httpCode;
                error = new ApiError(context.Exception, ex.StatusCode);
            }
            else
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                error = new ApiError(context.Exception, ErrorType.Unknown);
            }

            if (isValidationException)
            {
                _logger.Warning(error.ToString());
            }
            else
            {
                _logger.Error(error.ToString());
            }

            context.Result = new ObjectResult(error);
            base.OnException(context);
        }
    }
}
