﻿using Identity.Domain;
using System.Diagnostics.CodeAnalysis;

namespace IdentityManagementSystem.Core
{
    /// <summary>
    /// The application settings.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Settings
    {
        /// <summary>
        /// The connection string.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// The auth settings.
        /// </summary>
        public AuthSettings Auth { get; set; }
    }
}
