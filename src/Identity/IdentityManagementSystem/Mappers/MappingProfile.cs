﻿using AutoMapper;
using Identity.BusinessLogic.Models;
using Identity.Domain.Models;
using IdentityManagementSystem.Models;
using IdentityManagementSystem.Models.Requests;
using IdentityManagementSystem.Models.Responses;

namespace IdentityManagementSystem.Mappers
{
    /// <summary>
    /// The mapping profile.
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes mapping profiles.
        /// </summary>
        public MappingProfile()
        {
            CreateMap<RegistrationRequest, User>();
            CreateMap<LoginRequest, User>();
            CreateMap<RoleModel, RoleResponse>();
            CreateMap<RoleRequest, RoleModel>();
            CreateMap<Permission, PermissionModel>();
            CreateMap<PermissionModel, Permission>()
                .ForMember("Path", opt => opt.MapFrom(x => x.Controller + ':' + x.Action));
        }
    }
}
