﻿using AutoMapper;
using Identity.BusinessLogic.Models;
using Identity.BusinessLogic.Services;
using IdentityManagementSystem.ErrorHandling;
using IdentityManagementSystem.Models.Requests;
using IdentityManagementSystem.Models.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IdentityManagementSystem.Controllers
{
    /// <summary>
    /// The role controller.
    /// </summary>
    [ApiController]
    [Route("api/v{version:apiVersion}/roles")]
    [ApiVersion("1.0")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes the controller.
        /// </summary>
        /// <param name="roleService">The role service.</param>
        /// <param name="mapper">The mapper.</param>
        public RoleController(
            IRoleService roleService,
            IMapper mapper)
        {
            _mapper = mapper;
            _roleService = roleService;
        }

        /// <summary>
        /// Gets the role with permissions.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <response code="200">The role with permissions.</response>
        /// <returns>The role.</returns>
        [HttpGet]
        [Route("{roleId}")]
        [ProducesResponseType(typeof(RoleResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public IActionResult Get(int roleId)
        {
            var role = _roleService.GetById(roleId);
            var roleModel = _mapper.Map<RoleResponse>(role);
            return Ok(roleModel);
        }

        /// <summary>
        /// Adds new role with related permissions.
        /// </summary>
        /// <param name="request">The request with role and permissions information.</param>
        /// <response code="200">The role was created.</response>
        /// <response code="400">The bad request. Possible item information wrong.</response>
        /// <returns>The result role.</returns>
        [HttpPost]
        [Route("")]
        [ProducesResponseType(typeof(List<RoleResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Add([FromBody] RoleRequest request)
        {
            var roleModel = _mapper.Map<RoleModel>(request);
            var addedRole = await _roleService.Add(roleModel);
            var responseRole = _mapper.Map<RoleResponse>(addedRole);
            return Ok(responseRole);
        }

        /// <summary>
        /// Updates role information with related permissions.
        /// </summary>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="request">The request with role and permissions.</param>
        /// <response code="200">The role was updated.</response>
        /// <response code="404">The role was not found.</response>
        /// <response code="400">The bad request. Possible item information wrong.</response>
        /// <returns>The updated cart.</returns>
        [HttpPut]
        [Route("{roleId}")]
        [ProducesResponseType(typeof(List<RoleResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update(int roleId, [FromBody] RoleRequest request)
        {
            var roleModel = _mapper.Map<RoleModel>(request);
            roleModel.Id = roleId;
            var updatedRole = await _roleService.Update(roleModel);
            var responseRole = _mapper.Map<RoleResponse>(updatedRole);
            return Ok(responseRole);
        }

        /// <summary>
        /// Removes the role with related permissions.
        /// </summary>
        /// <param name="roleId">The identifier of role to delete.</param>
        /// <response code="200">The role was removed with related permissions.</response>
        /// <response code="404">The role does not found.</response>
        /// <returns>The updated cart.</returns>
        [HttpDelete]
        [Route("{roleId}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int roleId)
        {
            await _roleService.Delete(roleId);
            return Ok();
        }
    }
}
