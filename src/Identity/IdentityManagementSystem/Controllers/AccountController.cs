﻿using AutoMapper;
using Identity.BusinessLogic.Services;
using Identity.Domain.Models;
using IdentityManagementSystem.ErrorHandling;
using IdentityManagementSystem.Models.Requests;
using IdentityManagementSystem.Models.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IdentityManagementSystem.Controllers
{
    /// <summary>
    /// The account controller.
    /// </summary>
    [ApiController]
    [Route("api/v{version:apiVersion}/accounts")]
    [ApiVersion("1.0")]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ITokenService _tokenService;

        /// <summary>
        /// Initializes controller instance.
        /// </summary>
        /// <param name="userService">The user service.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="tokenService">The token service.</param>
        public AccountController(
            IUserService userService,
            ITokenService tokenService,
            IMapper mapper)
        {
            _mapper = mapper;
            _userService = userService;
            _tokenService = tokenService;
        }

        /// <summary>
        /// Register user.
        /// </summary>
        /// <response code="200">The user was created.</response>
        /// <response code="400">The wrong user information.</response>
        /// <returns>The result of user creation.</returns>
        [HttpPost]
        [Route("register")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Register([FromBody] RegistrationRequest userModel)
        {
            var user = _mapper.Map<User>(userModel);
            await _userService.Register(user);
            return Ok();
        }

        /// <summary>
        /// Login user.
        /// </summary>
        /// <param name="userModel">The user information</param>
        /// <response code="200">The user log in.</response>
        /// <response code="400">The wrong user information.</response>
        /// <returns>The token.</returns>
        [HttpPost]
        [Route("login")]
        [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        public IActionResult Login([FromBody] LoginRequest userModel)
        {
            var user = _mapper.Map<User>(userModel);
            var userRoleModel = _userService.Login(user);
            var token = _tokenService.CreateToken(userRoleModel);
            return Ok(token);
        }

        /// <summary>
        /// Verifies the access.
        /// </summary>
        /// <param name="request">The request with information for verification.</param>
        /// <returns>The result of validation.</returns>
        [HttpPost]
        [Route("verifyaccess")]
        [ProducesResponseType(typeof(LoginResponse), StatusCodes.Status200OK)]
        public IActionResult VerifyToken([FromBody]TokenVerifyRequest request)
        {
            var isTokenValid = _tokenService.IsTokenValid(request.Token, request.Path);
            return Ok(isTokenValid);
        }
    }
}
