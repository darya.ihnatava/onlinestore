﻿namespace IdentityManagementSystem.Models.Responses
{
    /// <summary>
    /// The login response.
    /// </summary>
    public class LoginResponse
    {
        /// <summary>
        /// The token.
        /// </summary>
        public string Token { get; set; }
    }
}
