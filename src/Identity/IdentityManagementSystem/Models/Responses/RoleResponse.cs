﻿using Identity.Domain.Models;
using System.Collections.Generic;

namespace IdentityManagementSystem.Models.Responses
{
    /// <summary>
    /// The role response.
    /// </summary>
    public class RoleResponse
    {
        /// <summary>
        /// THe role name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The permissions.
        /// </summary>
        public List<Permission> Permissions { get; set; }
    }
}
