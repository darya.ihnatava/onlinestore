﻿namespace IdentityManagementSystem.Models
{
    /// <summary>
    /// THe permissions model.
    /// </summary>
    public class PermissionModel
    {
        /// <summary>
        /// THe controller path.
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// THe action path.
        /// </summary>
        public string Action { get; set; }
    }
}
