﻿using System.ComponentModel.DataAnnotations;

namespace IdentityManagementSystem.Models.Requests
{
    /// <summary>
    /// The login request.
    /// </summary>
    public class LoginRequest
    {
        /// <summary>
        /// The user.
        /// </summary>
        [MinLength(5)]
        [MaxLength(20)]
        public string Name { get; set; }

        /// <summary>s
        /// The password.
        /// </summary>
        [MinLength(5)]
        [MaxLength(20)]
        public string Password { get; set; }
    }
}
