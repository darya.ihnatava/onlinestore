﻿
using System.ComponentModel.DataAnnotations;

namespace IdentityManagementSystem.Models.Requests
{
    /// <summary>
    /// The user model
    /// </summary>
    public class RegistrationRequest
    {
        /// <summary>
        /// The user.
        /// </summary>
        [MinLength(5)]
        [MaxLength(20)]
        public string Name { get; set; }

        /// <summary>s
        /// The password.
        /// </summary>
        [MinLength(5)]
        [MaxLength(20)]
        public string Password { get; set; }

        /// <summary>
        /// The role identifier.
        /// </summary>
        public int RoleId { get; set; }
    }
}
