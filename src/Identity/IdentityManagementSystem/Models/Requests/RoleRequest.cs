﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IdentityManagementSystem.Models.Requests
{
    /// <summary>
    /// The role request.
    /// </summary>
    public class RoleRequest
    {
        /// <summary>
        /// The role name.
        /// </summary>
        [MinLength(4)]
        [MaxLength(20)]
        public string Name { get; set; }

        /// <summary>
        /// The permission collection.
        /// </summary>
        public List<PermissionModel> Permissions { get; set; }
    }
}
