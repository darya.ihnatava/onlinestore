using API.Shared.Swagger;
using AutoMapper;
using Identity.BusinessLogic;
using Identity.DataAccess;
using Identity.Domain;
using IdentityManagementSystem.Core;
using IdentityManagementSystem.Mappers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using System;
using System.Threading.Tasks;
using IdentityManagementSystem.Middlewares;

namespace IdentityManagementSystem
{
    public class Startup
    {
        private const string ApiDescription = "Identity Management Systems is a system that provides authZ and authN.";
        private const string ApiTitle = "Identity Management Systems";
        private const string CorrelationIdHeader = "x-correlation-id";
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            InitializeLogger(services);
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            services.AddSingleton<IMapper>(mapperConfig.CreateMapper());

            services.Configure<Settings>(_configuration);
            services.AddTransient<ISettingsProvider, SettingsProvider>();

            services.AddServiceDependencies();
            services.AddRepositoryDependencies();
            services.AddControllers();
            services.AddSwaggerConfiguration(ApiTitle, ApiDescription);
            services.AddVersionedApiExplorer(opt => opt.GroupNameFormat = "'v'VVV");

            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
                config.ApiVersionReader = ApiVersionReader.Combine(new HeaderApiVersionReader("x-version"), new UrlSegmentApiVersionReader());
            });

            services.AddMvcCore(options => options.Filters.Add<ApiExceptionFilterAttribute>())
                    .AddApiExplorer()
                    .AddDataAnnotations();
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="apiVersionDescriptionProvider">The API version description provider.</param>
        /// <param name="serviceProvider">The service provider.</param>
        public async Task Configure(
            IApplicationBuilder app,
            IApiVersionDescriptionProvider apiVersionDescriptionProvider,
            IServiceProvider serviceProvider)
        {
            var loggerFactory = serviceProvider.GetService<ILoggerFactory>();
            loggerFactory.AddSerilog();

            app.UseDeveloperExceptionPage();

            app.UseConfiguredSwaggerInApp(apiVersionDescriptionProvider);
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseMiddleware<CorrelationIdMiddleware>();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void InitializeLogger(IServiceCollection services)
        {
            if (Log.Logger == null)
            {
                const string exceptionMessage = "Logger instance was not initialized before registering into DI system";
                throw new InvalidOperationException(exceptionMessage);
            }

            Log.Logger = new LoggerConfiguration()
                    .ReadFrom
                    .Configuration(_configuration, "Serilog")
                    .Enrich.WithCorrelationIdHeader(CorrelationIdHeader)
                    .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Warning)
                    .CreateLogger();
            services.AddSingleton(Log.Logger);
        }
    }
}
