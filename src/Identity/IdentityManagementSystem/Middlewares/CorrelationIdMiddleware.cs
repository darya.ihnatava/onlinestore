﻿using System.Threading.Tasks;
using Identity.BusinessLogic.Exceptions;
using IdentityManagementSystem.Core;
using IdentityManagementSystem.ErrorHandling;
using Microsoft.AspNetCore.Http;
using Serilog;
using Serilog.Context;
using Serilog.Core;
using Serilog.Core.Enrichers;

namespace IdentityManagementSystem.Middlewares
{
    /// <summary>
    /// The Correlation Id middleware.
    /// </summary>
    public class CorrelationIdMiddleware
    {
        private const string CorrelationIdHeader = "x-correlation-id";
        private const string CorrelationIdDefinition = "CorrelationId";

        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="CorrelationIdMiddleware"/> class.
        /// </summary>
        public CorrelationIdMiddleware(RequestDelegate next, ILogger logger)
        {
            _next = next;
            _logger = logger;
        }

        /// <summary>
        /// Invokes the middleware.
        /// </summary>
        public async Task Invoke(HttpContext context)
        {
            if (!context.Request.Headers.TryGetValue(CorrelationIdHeader, out var correlationId))
            {
                var apiError = new ApiError(description: "Correlation Id is missing", code: ErrorType.BadRequest);
                _logger.Warning(apiError.Error.Message);
                await context.WriteErrorAsync(apiError);
                return;
            }

            context.TraceIdentifier = correlationId;
            context.Items[CorrelationIdDefinition] = correlationId;
            context.Response.Headers.Add(CorrelationIdHeader, correlationId);
            using (LogContext.Push(new ILogEventEnricher[] { new PropertyEnricher(CorrelationIdDefinition, correlationId) }))
            {
                await _next.Invoke(context);
            }
        }
    }
}
