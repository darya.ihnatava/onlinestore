﻿namespace Identity.Client
{
    /// <summary>
    /// The token verify request.
    /// </summary>
    public class TokenVerifyRequest
    {
        /// <summary>
        /// The token.
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// The requested path.
        /// </summary>
        public string Path { get; set; }
    }
}
