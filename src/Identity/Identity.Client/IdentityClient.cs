﻿using System.Threading.Tasks;

namespace Identity.Client
{
    /// <summary>
    /// THe identify client.
    /// </summary>
    public interface IIdentityClient
    {
        /// <summary>
        /// Verifies the token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="path">The path.</param>
        /// <returns>The result of verification.</returns>
        Task<bool> VerifyUserToken(string token, string path);
    }

    /// <inheritdoc />
    public class IdentityClient : IIdentityClient
    {
        private readonly IIdentityApi _identityApi;

        public IdentityClient(IIdentityApi identityApi)
        {
            _identityApi = identityApi;
        }

        /// <inheritdoc />
        public async Task<bool> VerifyUserToken(string token, string path)
        {
            var request = new TokenVerifyRequest
            {
                Token = token,
                Path = path
            };
            var result = await _identityApi.VerifyUserAccess(request);

            return result;
        }
    }
}
