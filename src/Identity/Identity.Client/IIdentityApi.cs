﻿using RestEase;
using System.Threading.Tasks;

namespace Identity.Client
{
    /// <summary>
    /// The identity client.
    /// </summary>
    public interface IIdentityApi
    {
        /// <summary>
        /// The correlation identifier.
        /// </summary>
        [Header("x-correlation-id")]
        string CorrelationId { get; set; }

        /// <summary>
        /// Verify is the access allowed for selected path.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>THe result of token verification.</returns>
        [Post("api/v1.0/accounts/verifyaccess")]
        Task<bool> VerifyUserAccess([Body] TokenVerifyRequest request);
    }
}
