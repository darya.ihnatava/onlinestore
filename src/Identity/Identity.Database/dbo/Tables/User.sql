﻿CREATE TABLE [dbo].[User]
(
	[Id]          INT                NOT NULL  PRIMARY KEY  IDENTITY(1, 1),
	[Name]        NVARCHAR(50)       NOT NULL,
	[Password]    NVARCHAR(MAX)      NOT NULL,
	[RoleId]      INT                NOT NULL CONSTRAINT FK_User$Role FOREIGN KEY REFERENCES [dbo].[Role] ([Id])
)