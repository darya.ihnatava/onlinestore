﻿CREATE TABLE [dbo].[Permission]
(
	[Id]            INT              NOT NULL PRIMARY KEY IDENTITY(1, 1),
	[Path]          NVARCHAR(200)    NOT NULL,
	[RoleId]        INT              NOT NULL CONSTRAINT FK_Permission$Role FOREIGN KEY REFERENCES [dbo].[Role] ([Id]) ON DELETE CASCADE
)