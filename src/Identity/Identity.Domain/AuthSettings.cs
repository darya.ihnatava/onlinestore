﻿namespace Identity.Domain
{
    /// <summary>
    /// The auth settings.
    /// </summary>
    public class AuthSettings
    {
        /// <summary>
        /// The minutes of token expiration.
        /// </summary>
        public int TokenExpirationInMinutes { get; set; }

        /// <summary>
        /// The issuer.
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// THe key.
        /// </summary>
        public string Key { get; set; }
    }
}
