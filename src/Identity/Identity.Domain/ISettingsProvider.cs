﻿namespace Identity.Domain
{
    /// <summary>
    /// The settings provider.
    /// </summary>
    public interface ISettingsProvider
    {
        /// <summary>
        /// Gets the database connection string.
        /// </summary>
        /// <returns>The connection string.</returns>
        string GetDatabaseConnectionString();

        /// <summary>
        /// Gets the settings of the auth.
        /// </summary>
        /// <returns>The settings.</returns>
        AuthSettings GetAuthSettings();
    }
}
