﻿namespace Identity.Domain.Models
{
    /// <summary>
    /// The user entity.
    /// </summary>
    public class User : IModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The user name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The role identifier.
        /// </summary>
        public int RoleId { get; set; }
    }
}
