﻿namespace Identity.Domain.Models
{
    /// <summary>
    /// The permission.
    /// </summary>
    public class Permission : IModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The path of allowed resource.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// The role identifier.
        /// </summary>
        public int RoleId { get; set; }
    }
}
