﻿namespace Identity.Domain.Models
{
    /// <summary>
    /// The role.
    /// </summary>
    public class Role : IModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name.
        /// </summary>
        public string Name { get; set; }
    }
}
