﻿namespace Identity.Domain.Models
{
    /// <summary>
    /// The model interface.
    /// </summary>
    public interface IModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        int Id { get; set; }
    }
}
