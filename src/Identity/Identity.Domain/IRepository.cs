﻿using Identity.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Domain
{
    /// <summary>
    /// The repository main interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T>
        where T : IModel
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>The collection of items.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The item.</returns>
        Task<T> GetById(int id);

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The item.</returns>
        Task<T> Add(T item);

        /// <summary>
        /// Adds the item's collection.
        /// </summary>
        /// <param name="item">The list of items.</param>
        /// <returns>The collection.</returns>
        Task<List<T>> AddRange(List<T> list);

        /// <summary>
        /// Updates the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The item.</returns>
        Task<T> Update(T item);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        Task Delete(int itemId);

        /// <summary>
        /// Deletes range of the items.
        /// </summary>
        /// <param name="list">THe collection.</param>
        Task DeleteRange(List<T> list);
    }
}
