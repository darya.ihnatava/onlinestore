﻿using System.Linq;
using System.Threading.Tasks;

namespace Catalog.Domain.Dependencies
{
    /// <summary>
    /// The repository main interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T>
        where T : IModel
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>The collection of items.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The item.</returns>
        Task<T> GetById(int id);

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The item.</returns>
        Task<T> Add(T item);

        /// <summary>
        /// Updates the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The item.</returns>
        Task<T> Update(T item);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        Task Delete(int itemId);
    }
}
