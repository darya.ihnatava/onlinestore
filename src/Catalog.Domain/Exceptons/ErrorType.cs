﻿namespace Catalog.Domain.Exceptons
{
    /// <summary>
    /// The error type.
    /// </summary>
    public enum ErrorType
    {
        /// <summary>
        /// Unknown error.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Instance not found.
        /// </summary>
        InstanceNotExist = 1,

        /// <summary>
        /// No linked entity for creation/update.
        /// </summary>
        LinkedInstanceNotExist = 2,

        /// <summary>
        /// Can not remove instance as it has linked objects.
        /// </summary>
        InstanceHasLinkedObjects = 3,

        /// <summary>
        /// The not token used.
        /// </summary>
        NotAuthorized = 4,

        /// <summary>
        /// No access.
        /// </summary>
        Forbidden = 5,

        /// <summary>
        /// The bad request.
        /// </summary>
        BadRequest = 6
    }
}
