﻿using System.Diagnostics.CodeAnalysis;

namespace Catalog.Domain.Models
{
    /// <summary>
    /// The Category entity.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Product : IModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The category identifier.
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// The price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// The amount.
        /// </summary>
        public int Amount { get; set; }
    }
}
