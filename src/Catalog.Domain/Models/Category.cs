﻿using System.Diagnostics.CodeAnalysis;

namespace Catalog.Domain.Models
{
    /// <summary>
    /// The Category entity.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Category : IModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The parent category identifier.
        /// </summary>
        public int? ParentCategoryId { get; set; }
    }
}
