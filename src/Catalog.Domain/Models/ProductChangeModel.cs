﻿namespace Catalog.Domain.Models
{
    /// <summary>
    /// The model of the product change.
    /// </summary>
    public class ProductChangeModel
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// THe quantity.
        /// </summary>
        public int Quantity { get; set; }
    }
}
