﻿using Catalog.API.Models.Product.Create;
using Catalog.API.Models.Product.Get;
using Catalog.API.Models.Product.Management;
using Catalog.BusinessLogic.Services;
using GraphQL;
using GraphQL.Types;

namespace Catalog.API.Models.Product
{
    public class ProductMutation : ObjectGraphType<object>
    {
        public ProductMutation(IProductService productService)
        {
            Name = "ProductMutation";
            Field<ProductAddModel>(
                "createProduct",
                arguments:
                new QueryArguments(new QueryArgument<NonNullGraphType<ProductCreateType>> { Name = "product" }),
                resolve: context =>
                {
                    var productInput = context.GetArgument<ProductCreateInput>("product");
                    var product = new Domain.Models.Product
                    {
                        Name = productInput.Name,
                        Description = productInput.Description,
                        Price = productInput.Price,
                        ImageUrl = productInput.ImageUrl,
                        CategoryId = productInput.CategoryId,
                        Amount = productInput.Amount
                    };
                    return productService.Add(product);
                });

            Field<ProductAddModel>(
                "updateProduct",
                arguments:
                new QueryArguments(
                    new QueryArgument<NonNullGraphType<ProductCreateType>> { Name = "product" },
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "productId" }),
                resolve: context =>
                {
                    var productInput = context.GetArgument<ProductCreateInput>("product");
                    var productId = context.GetArgument<int>("productId");
                    var product = new Domain.Models.Product
                    {
                        Id = productId,
                        Name = productInput.Name,
                        Description = productInput.Description,
                        Price = productInput.Price,
                        ImageUrl = productInput.ImageUrl,
                        CategoryId = productInput.CategoryId,
                        Amount = productInput.Amount
                    };
                    return productService.Update(product);
                });


            Field<IdGraphType>(
                "deleteProduct",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "productId" }),
                resolve: context =>
                {
                    var productId = context.GetArgument<int>("productId");
                    productService.Delete(productId);
                    return $"The product with {productId} was removed successfully.";
                });
        }
    }
}