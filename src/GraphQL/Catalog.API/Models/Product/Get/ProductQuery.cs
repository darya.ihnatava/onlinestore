﻿using Catalog.BusinessLogic.Services;
using GraphQL;
using GraphQL.Types;

namespace Catalog.API.Models.Product.Get
{
    public class ProductQuery : ObjectGraphType<object>
    {
        public ProductQuery(IProductService productService)
        {
            Name = "ProductQuery";
            Field<ListGraphType<ProductModel>>(
                "products",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = "categoryId" },
                    new QueryArgument<IdGraphType> { Name = "itemsOnPageCount" },
                    new QueryArgument<IdGraphType> { Name = "pageNumber" }
                ),
                resolve: context =>
                {
                    var categoryId = context.GetArgument<int?>("categoryId");
                    var itemsOnPageCount = context.GetArgument<int?>("itemsOnPageCount");
                    var pageNumber = context.GetArgument<int?>("pageNumber");
                    if (itemsOnPageCount != null && pageNumber != null)
                    {
                        return productService.GetAllByCategoryIdWithPagination(categoryId, itemsOnPageCount.Value, pageNumber.Value);
                    }

                    return productService.GetAll();
                });
        }
    }
}
