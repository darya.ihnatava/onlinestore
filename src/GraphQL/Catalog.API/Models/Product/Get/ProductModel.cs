﻿using Catalog.BusinessLogic.Services;
using GraphQL.Types;

namespace Catalog.API.Models.Product.Get
{
    /// <summary>
    /// The product model.
    /// </summary>
    public class ProductModel : ObjectGraphType<Domain.Models.Product>
    {
        public ProductModel(ICategoryService categoryService)
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.Amount);
            Field(x => x.Description);
            Field(x => x.ImageUrl);
            Field(x => x.Price);
            Field<CategoryWithoutParentModel>("category", resolve: context => categoryService.GetById(context.Source.CategoryId));
        }
    }
}
