﻿using GraphQL.Types;

namespace Catalog.API.Models.Product.Get
{
    /// <summary>
    /// The category model.
    /// </summary>
    public class CategoryWithoutParentModel : ObjectGraphType<Domain.Models.Category>
    {
        public CategoryWithoutParentModel()
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.ImageUrl);
        }
    }
}
