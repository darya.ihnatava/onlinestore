﻿using System;
using Catalog.API.Models.Product.Create;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace Catalog.API.Models.Product.Get
{
    public class ProductScheme : Schema
    {
        public ProductScheme(IServiceProvider serviceProvider, ProductMutation mutation)
        : base(serviceProvider)
        {
            Query = serviceProvider.GetRequiredService<ProductQuery>();
            Mutation = mutation;
        }
    }
}
