﻿using GraphQL;
using GraphQL.Types;

namespace Catalog.API.Models.Product.Create
{
    public class ProductCreateType : InputObjectGraphType
    {
        public ProductCreateType()
        {
            Name = "ProductInput";
            Field<NonNullGraphType<StringGraphType>>(nameof(ProductCreateInput.Name).ToCamelCase());
            Field<NonNullGraphType<StringGraphType>>(nameof(ProductCreateInput.Description).ToCamelCase());
            Field<NonNullGraphType<StringGraphType>>(nameof(ProductCreateInput.ImageUrl).ToCamelCase());
            Field<NonNullGraphType<IntGraphType>>(nameof(ProductCreateInput.Amount).ToCamelCase());
            Field<NonNullGraphType<IntGraphType>>(nameof(ProductCreateInput.CategoryId).ToCamelCase());
            Field<NonNullGraphType<DecimalGraphType>>(nameof(ProductCreateInput.Price).ToCamelCase());
        }
    }
}
