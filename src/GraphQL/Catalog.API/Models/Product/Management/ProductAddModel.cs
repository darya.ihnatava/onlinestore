﻿using GraphQL.Types;

namespace Catalog.API.Models.Product.Management
{
    /// <summary>
    /// The category model.
    /// </summary>
    public class ProductAddModel : ObjectGraphType<Domain.Models.Product>
    {
        public ProductAddModel()
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.Amount);
            Field(x => x.Description);
            Field(x => x.ImageUrl);
            Field(x => x.Price);
            Field(x => x.CategoryId);
        }
    }
}
