﻿using Catalog.BusinessLogic.Services;
using GraphQL.Types;

namespace Catalog.API.Models.Product.Management
{
    public class ProductAddQuery : ObjectGraphType<object>
    {
        public ProductAddQuery(IProductService productService)
        {
            Name = "ProductManagementQuery";
            Field<ListGraphType<ProductAddModel>>(
                "products",
                resolve: context => productService.GetAll());
        }
    }
}
