﻿using System;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace Catalog.API.Models.Product.Management
{
    public class ProductAddScheme : Schema
    {
        public ProductAddScheme(IServiceProvider serviceProvider, ProductMutation mutation)
        : base(serviceProvider)
        {
            Query = serviceProvider.GetRequiredService<ProductAddQuery>();
            Mutation = mutation;
        }
    }
}
