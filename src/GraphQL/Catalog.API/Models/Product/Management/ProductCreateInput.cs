﻿namespace Catalog.API.Models.Product.Create
{
    /// <summary>
    /// Product create object.
    /// </summary>
    public class ProductCreateInput
    {
        /// <summary>
        /// The name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The category identifier.
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// The price.
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// The amount.
        /// </summary>
        public int Amount { get; set; }
    }
}
