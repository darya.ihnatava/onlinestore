﻿using Catalog.API.Models.Category.Management;
using Catalog.BusinessLogic.Services;
using GraphQL;
using GraphQL.Types;

namespace Catalog.API.Models.Category
{
    public class CategoryMutation : ObjectGraphType<object>
    {
        public CategoryMutation(ICategoryService categoryService)
        {
            Name = "CategoryMutation";
            Field<CategoryAddModel>(
                "createCategory",
                arguments:
                new QueryArguments(new QueryArgument<NonNullGraphType<CategoryCreateType>> { Name = "category" }),
                resolve: context =>
                {
                    var categoryInput = context.GetArgument<CategoryCreateInput>("category");
                    var product = new Domain.Models.Category
                    {
                        Name = categoryInput.Name,
                        ImageUrl = categoryInput.ImageUrl,
                        ParentCategoryId = categoryInput.ParentCategoryId,
                    };
                    return categoryService.Add(product);
                });

            Field<CategoryAddModel>(
                "updateCategory",
                arguments:
                new QueryArguments(new QueryArgument<NonNullGraphType<CategoryCreateType>> { Name = "category" },
                new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "categoryId" }),
                resolve: context =>
                {
                    var categoryInput = context.GetArgument<CategoryCreateInput>("category");
                    var categoryId = context.GetArgument<int>("categoryId");
                    var product = new Domain.Models.Category
                    {
                        Id = categoryId,
                        Name = categoryInput.Name,
                        ImageUrl = categoryInput.ImageUrl,
                        ParentCategoryId = categoryInput.ParentCategoryId,
                    };
                    return categoryService.Update(product);
                });

            Field<IdGraphType>(
                "deleteCategory",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "categoryId" }),
                resolve: context =>
                {
                    var categoryId = context.GetArgument<int>("categoryId");
                    categoryService.Delete(categoryId);
                    return $"The category with {categoryId} was removed successfully.";
                });
        }
    }
}