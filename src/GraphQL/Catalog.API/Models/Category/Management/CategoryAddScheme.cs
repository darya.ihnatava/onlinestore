﻿using System;
using Catalog.API.Models.Category.Get;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace Catalog.API.Models.Category.Management
{
    public class CategoryAddScheme : Schema
    {
        public CategoryAddScheme(IServiceProvider serviceProvider, CategoryMutation mutation)
        : base(serviceProvider)
        {
            Query = serviceProvider.GetRequiredService<CategoryQuery>();
            Mutation = mutation;
        }
    }
}
