﻿using Catalog.BusinessLogic.Services;
using GraphQL.Types;

namespace Catalog.API.Models.Category.Management
{
    public class CategoryAddQuery : ObjectGraphType<object>
    {
        public CategoryAddQuery(ICategoryService categoryService)
        {
            Name = "CategoryAddQuery";
            Field<ListGraphType<CategoryAddModel>>(
                "categories",
                resolve: context => categoryService.GetAll());
        }
    }
}
