﻿using GraphQL.Types;

namespace Catalog.API.Models.Category.Management
{
    /// <summary>
    /// The category model.
    /// </summary>
    public class CategoryAddModel : ObjectGraphType<Domain.Models.Category>
    {
        public CategoryAddModel()
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.ImageUrl);
            Field(x => x.ParentCategoryId, nullable: true);
        }
    }
}
