﻿using GraphQL;
using GraphQL.Types;

namespace Catalog.API.Models.Category.Management
{
    public class CategoryCreateType : InputObjectGraphType<CategoryCreateInput>
    {
        public CategoryCreateType()
        {
            Name = "CategoryInput";
            Field<NonNullGraphType<StringGraphType>>(nameof(CategoryCreateInput.Name).ToCamelCase());
            Field<NonNullGraphType<StringGraphType>>(nameof(CategoryCreateInput.ImageUrl).ToCamelCase());
            Field(x => x.ParentCategoryId, nullable: true, type: typeof(IntGraphType));
        }
    }
}
