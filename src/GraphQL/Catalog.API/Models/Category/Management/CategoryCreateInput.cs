﻿namespace Catalog.API.Models.Category.Management
{
    /// <summary>
    /// Product create object.
    /// </summary>
    public class CategoryCreateInput
    {
        /// <summary>
        /// The name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The parent category identifier.
        /// </summary>
        public int? ParentCategoryId { get; set; }
    }
}
