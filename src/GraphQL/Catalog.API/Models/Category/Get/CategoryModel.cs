﻿using Catalog.BusinessLogic.Services;
using GraphQL.Types;

namespace Catalog.API.Models.Category.Get
{
    /// <summary>
    /// The category model.
    /// </summary>
    public class CategoryModel : ObjectGraphType<Domain.Models.Category>
    {
        private const string ParentCategoryFieldName = "parent_category";

        public CategoryModel(ICategoryService categoryService)
        {
            Field(x => x.Id);
            Field(x => x.Name);
            Field(x => x.ImageUrl);
            Field<CategoryModel>(ParentCategoryFieldName,
                resolve: context => context.Source.ParentCategoryId == null
                    ? null
                    : categoryService.GetById(context.Source.ParentCategoryId.Value));
        }
    }
}
