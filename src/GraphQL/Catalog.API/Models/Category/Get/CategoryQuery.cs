﻿using Catalog.BusinessLogic.Services;
using GraphQL.Types;

namespace Catalog.API.Models.Category.Get
{
    public class CategoryQuery : ObjectGraphType<object>
    {
        public CategoryQuery(ICategoryService categoryService)
        {
            Name = "CategoryQuery";
            Field<ListGraphType<CategoryModel>>(
                "categories",
                resolve: context => categoryService.GetAll());
        }
    }
}
