﻿using System;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace Catalog.API.Models.Category.Get
{
    public class CategoryScheme : Schema
    {
        public CategoryScheme(IServiceProvider serviceProvider, CategoryMutation mutation)
        : base(serviceProvider)
        {
            Query = serviceProvider.GetRequiredService<CategoryQuery>();
            Mutation = mutation;
        }
    }
}
