﻿using Catalog.Domain.Dependencies;
using Microsoft.Extensions.Options;

namespace Catalog.API.Core
{
    /// <summary>
    /// The application settings.
    /// </summary>
    public class SettingsProvider : ISettingsProvider
    {
        private readonly IOptions<Settings> _options;

        /// <summary>
        /// The settings provider.
        /// </summary>
        /// <param name="options">The options.</param>
        public SettingsProvider(IOptions<Settings> options)
        {
            _options = options;
        }

        public string GetDatabaseConnectionString()
        {
            return _options.Value.ConnectionString;
        }
    }
}
