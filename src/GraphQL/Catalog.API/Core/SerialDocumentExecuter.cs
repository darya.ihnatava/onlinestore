﻿using System;
using GraphQL;
using GraphQL.Execution;
using GraphQL.Language.AST;

namespace Catalog.API.Core
{
    public class SerialDocumentExecuter : DocumentExecuter
    {
        private static IExecutionStrategy ParallelExecutionStrategy = new ParallelExecutionStrategy();
        private static IExecutionStrategy SerialExecutionStrategy = new SerialExecutionStrategy();
        private static IExecutionStrategy SubscriptionExecutionStrategy = new SubscriptionExecutionStrategy();

        protected override IExecutionStrategy SelectExecutionStrategy(GraphQL.Execution.ExecutionContext context)
        {
            return context.Operation.OperationType switch
            {
                OperationType.Query => SerialExecutionStrategy,
                OperationType.Mutation => SerialExecutionStrategy,
                OperationType.Subscription => SubscriptionExecutionStrategy,
                _ => throw new InvalidOperationException($"Unexpected OperationType {context.Operation.OperationType}"),
            };
        }
    }
}
