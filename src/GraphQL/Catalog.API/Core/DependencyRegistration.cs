﻿using System;
using Catalog.API.Models.Category.Get;
using Catalog.API.Models.Category.Management;
using Catalog.API.Models.Product.Get;
using GraphQL;
using GraphQL.Server;
using GraphQL.SystemTextJson;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Catalog.API.Core
{
    public static class DependencyRegistration
    {
        [Obsolete]
        public static void SetupGraphQlDependencies(this IServiceCollection services)
        {
            services
                .AddGraphQL((options, provider) =>
                {
                    var logger = provider.GetRequiredService<Serilog.ILogger>();
                    options.UnhandledExceptionDelegate = context => logger.Error("{Error} occurred", context.OriginalException.Message);
                })
                .AddGraphTypes(ServiceLifetime.Transient)
                .AddDataLoader()
                .AddSystemTextJson()
                .AddErrorInfoProvider(opt => opt.ExposeExceptionStackTrace = true);
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IDocumentExecuter, SerialDocumentExecuter>();
            services.AddTransient<IDocumentWriter, DocumentWriter>();


            services.AddTransient<CategoryScheme>();
            services.AddTransient<CategoryQuery>();

            services.AddTransient<CategoryAddScheme>();
            services.AddTransient<CategoryAddQuery>();

            services.AddTransient<ProductScheme>();
            services.AddTransient<ProductQuery>();

            services.AddTransient<IServiceProvider>(
                context => new FuncServiceProvider(context.GetRequiredService));
        }
    }
}
