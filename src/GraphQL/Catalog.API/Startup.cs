using System;
using Catalog.API.Core;
using Catalog.API.Models;
using Catalog.API.Models.Category.Get;
using Catalog.API.Models.Category.Management;
using Catalog.API.Models.Product.Get;
using Catalog.BusinessLogic;
using Catalog.DataAccess;
using Catalog.Domain.Dependencies;
using GraphQL;
using GraphQL.Server;
using GraphQL.SystemTextJson;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace Catalog.API
{
    /// <summary>
    /// The main configuration of the api.
    /// </summary>
    public partial class Startup
    {
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            InitializeLogger(services);
            services.AddServiceDependencies();
            services.AddRepositoryDependencies(); 
            services.Configure<Settings>(_configuration);
            services.AddTransient<ISettingsProvider, SettingsProvider>();
            var connectionString = services.BuildServiceProvider().GetService<ISettingsProvider>().GetDatabaseConnectionString();
            services.AddDbContext<Context>(options => options.UseSqlServer(connectionString), Microsoft.Extensions.DependencyInjection.ServiceLifetime.Transient);
            services.SetupGraphQlDependencies();
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="serviceProvider">The service provider.</param>
        public void Configure(
            IApplicationBuilder app,
            IServiceProvider serviceProvider)
        {
            var loggerFactory = serviceProvider.GetService<ILoggerFactory>();
            loggerFactory.AddSerilog();
            app.UseDeveloperExceptionPage();
            
            app.UseHttpsRedirection();
            app.UseRouting(); 

            app.UseGraphQL<ProductScheme>();
            app.UseGraphQL<CategoryScheme>();
            app.UseGraphQL<CategoryAddScheme>();
            app.UseGraphQLAltair("/");
        }

        private void InitializeLogger(IServiceCollection services)
        {
            if (Log.Logger == null)
            {
                const string exceptionMessage = "Logger instance was not initialized before registering into DI system";
                throw new InvalidOperationException(exceptionMessage);
            }

            Log.Logger = new LoggerConfiguration()
                    .ReadFrom
                    .Configuration(_configuration, "Serilog")
                    .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Warning)
                    .CreateLogger();
            services.AddSingleton(Log.Logger);
        }
    }
}
