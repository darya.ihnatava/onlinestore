﻿using System.Threading.Tasks;
using Catalog.Domain.Models;

namespace Kafka.Client
{
    /// <summary>
    /// The message producer of change.
    /// </summary>
    public interface IProducer
    {
        /// <summary>
        /// Produces the change.
        /// </summary>
        /// <param name="change">The product change model.</param>
        /// <param name="correlationId">The correlation id.</param>
        Task ProduceAsync(ProductChangeModel change, string correlationId);
    }
}
