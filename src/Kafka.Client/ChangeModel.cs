﻿using System;
using Catalog.Domain.Models;

namespace Kafka.Client
{
    /// <summary>
    /// THe change model.
    /// </summary>
    public class ChangeModel
    {
        /// <summary>
        /// THe product change.
        /// </summary>
        public ProductChangeModel ProductChange { get; set; }

        /// <summary>
        /// THe correlation Id.
        /// </summary>
        public string CorrelationId { get; set; }
    }
}
