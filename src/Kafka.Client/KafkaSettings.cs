﻿using Kafka.Base.Core;

namespace Kafka.Client
{
    /// <summary>
    /// The kafka settings.
    /// </summary>
    public class KafkaSettings : KafkaSettingsBase
    {
        /// <summary>
        /// THe attempts count.
        /// </summary>
        public int Attempts { get; set; }
    }
}
