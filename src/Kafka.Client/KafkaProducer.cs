﻿using System;
using Catalog.Domain.Models;
using Confluent.Kafka;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Kafka.Base.Core;
using Kafka.Base;
using Kafka.Base.Handlers;
using Microsoft.ApplicationInsights;
using Serilog;

namespace Kafka.Client
{
    public class KafkaProducer : KafkaClientBase, IProducer
    {
        private readonly IKafkaClientProvider<Null, ChangeModel> _clientProvider;
        private readonly KafkaSettings _settings;
        private readonly TelemetryClient _telemetryClient;

        private ProducerConfig _config;
        private ProducerHandlers<Null, ChangeModel> _handlers;
        private IProducer<Null, ChangeModel> _producer;

        /// <summary>
        /// Initializes kafka producer instance.
        /// </summary>
        /// <param name="clientProvider">The client provider.</param>
        /// <param name="settings">The settings.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="telemetryClient">The telemetry client.</param>
        public KafkaProducer(
            IKafkaClientProvider<Null, ChangeModel> clientProvider,
            IOptions<KafkaSettings> settings,
            TelemetryClient telemetryClient,
            ILogger logger)
            : base(logger, settings.Value)
        {
            _settings = settings.Value;
            _clientProvider = clientProvider;
            _telemetryClient = telemetryClient;
            InitialSetup();
        }

        private void InitialSetup()
        {
            _config = new ProducerConfig
            {
                BootstrapServers = _settings.BootstrapServers,
                RequestTimeoutMs = _settings.Timeout
            };
            _handlers = new ProducerHandlers<Null, ChangeModel>
            {
                ErrorHandler = (_, error) => Logger.Error($"An error appeared on kafka side. Error details:{error}"),
                LogHandler = (_, message) => Logger.Debug(message.Message)
            };
            _producer = _clientProvider.CreateProducer(_config, _handlers);
        }

        /// <inheritdoc />
        public async Task ProduceAsync(ProductChangeModel model, string correlationId)
        {
            var attempt = 0;
            var isSuccess = true;
            do
            {
                var changeModel = new ChangeModel
                {
                    ProductChange = model,
                    CorrelationId = correlationId
                };
                var startTime = DateTime.UtcNow;
                var timer = System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    attempt++;
                    var task = _producer.ProduceAsync(_settings.Topic, new Message<Null, ChangeModel> { Value = changeModel });
                    await HandleInternalTask(task);
                    return;
                }
                catch (KafkaTimeoutException ex)
                {
                    Dispose();
                    _producer = _clientProvider.CreateProducer(_config, _handlers);
                    Logger.Error(ex, ex.Message);
                    isSuccess = false;
                }
                finally
                {
                    timer.Stop();
                    _telemetryClient.TrackDependency("Kafka", "Produce", model.Id.ToString(), startTime, timer.Elapsed, isSuccess);
                }
            } while (_settings.Attempts > attempt);
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            _producer.Flush();
            _producer.Dispose();
        }
    }
}