﻿using Cart.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Cart.Domain.Exceptions;
using Cart.Domain.Dependencies;

namespace Cart.BusinessLogic
{
    /// <summary>
    /// The Cart service.
    /// </summary>
    public interface ICartService
    {
        /// <summary>
        /// Getting item by identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        /// <returns>The cart with collection of cart items.</returns>
        Domain.Models.Cart GetById(Guid cartId);

        /// <summary>
        /// Adds item to cart by identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        /// <param name="cartItem">The cart item.</param>
        Domain.Models.Cart AddItem(Guid cartId, CartItem cartItem);

        /// <summary>
        /// Removes item from cart by identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        /// <param name="itemId">The item identifier.</param>
        Domain.Models.Cart RemoveItem(Guid cartId, int itemId);

        /// <summary>
        /// Updates carts items with new product information.
        /// </summary>
        /// <param name="productChange">The change of the product information.</param>
        void UpdateCartsByChangedProductData(ProductChangeModel productChange);
    }

    /// <inheritdoc />
    internal class CartService : ICartService
    {
        private readonly ICartRepository _repository;

        public CartService(ICartRepository repository)
        {
            _repository = repository;
        }

        /// <inheritdoc />
        public Domain.Models.Cart GetById(Guid cartId)
        {
            var cart = GetByIdWithValidation(cartId);
            return cart;
        }

        /// <inheritdoc />
        public Domain.Models.Cart AddItem(Guid cartId, CartItem cartItem)
        {
            var cart = _repository.GetById(cartId);
            if (cart == null)
            {
                return CreateNewCart(cartId, cartItem);
            }

            InsureItemNotExists(cart, cartItem.Id);

            cart.Items.Add(cartItem);
            var updatedCart = _repository.Update(cart);
            return updatedCart;
        }

        /// <inheritdoc />
        public Domain.Models.Cart RemoveItem(Guid cartId, int itemId)
        {
            var cart = GetByIdWithValidation(cartId);
            var cartItem = GetCartItemWithValidation(cart, itemId);

            cart.Items.Remove(cartItem);
            var updatedCart = _repository.Update(cart);
            return updatedCart;
        }

        /// <inheritdoc />
        public void UpdateCartsByChangedProductData(ProductChangeModel productChange)
        {
            var cartsToUpdate = _repository.GetCartsByProductId(productChange.Id);
            foreach (var cart in cartsToUpdate)
            {
                var toUpdate = cart.Items.Where(x => x.Id == productChange.Id).ToList();
                toUpdate.ForEach(x =>
                {
                    x.Name = productChange.Name;
                    x.Price = productChange.Price;
                    x.ImageUrl = productChange.ImageUrl;
                    x.Quantity = x.Quantity > productChange.Quantity ? productChange.Quantity : x.Quantity;
                });
                _repository.Update(cart);
            }
        }

        private Domain.Models.Cart GetByIdWithValidation(Guid cartId)
        {
            var cart = _repository.GetById(cartId);
            if (cart == null)
            {
                throw new ServiceException($"There are no cart {cartId}.", ErrorType.InstanceNotExist);
            }

            return cart;
        }

        private Domain.Models.Cart CreateNewCart(Guid cartId, CartItem cartItem)
        {
            var cart = new Domain.Models.Cart
            {
                Id = cartId,
                Items = new List<CartItem> { cartItem }
            };
            return _repository.Create(cart);
        }

        private void InsureItemNotExists(Domain.Models.Cart cart, int cartItemId)
        {
            var isExists = cart.Items.Any(x => x.Id == cartItemId);
            if (isExists)
            {
                throw new ServiceException($"The item {cartItemId} already exists in cart {cart.Id}.", ErrorType.CollectionItemAlreadyExists);
            }
        }

        private CartItem GetCartItemWithValidation(Domain.Models.Cart cart, int cartItemId)
        {
            var cartItem = cart.Items.FirstOrDefault(x => x.Id == cartItemId);
            if (cartItem == null)
            {
                throw new ServiceException($"There are no item {cartItemId} in cart {cart.Id}.", ErrorType.CollectionItemNotExists);
            }

            return cartItem;
        }
    }
}
