﻿using Microsoft.Extensions.DependencyInjection;

namespace Cart.BusinessLogic
{
    /// <summary>
    /// The registration on business logic services.
    /// </summary>
    public static class BlModule
    {
        /// <summary>
        /// The method on business logic services.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void AddServiceDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICartService, CartService>();
        }
    }
}
