﻿using API.Shared.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;

namespace API.Shared.Swagger
{
    /// <summary>
    /// Extensions for Swagger.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class SwaggerExtensions
    {
        private const string CorrelationIdHeader = "x-correlation-id";
        private const string CorrelationIdDefinition = "CorrelationId";

        /// <summary>
        /// Initial Swagger setup.
        /// </summary>
        public static void AddSwaggerConfiguration(this IServiceCollection services, string apiTitle, string apiDescription)
        {
            services.AddSwaggerGen(options =>
            {
                options.UseOneOfForPolymorphism();
                options.UseAllOfForInheritance();
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(
                        description.GroupName,
                        new OpenApiInfo
                        {
                            Title = apiTitle,
                            Description = apiDescription,
                            Version = description.ApiVersion.ToString()
                        });
                }

                options.AddSecurityDefinition(CorrelationIdDefinition, new OpenApiSecurityScheme
                {
                    Description = CorrelationIdDefinition,
                    Name = CorrelationIdHeader,
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });

                options.DescribeAllParametersInCamelCase();
                options.OperationFilter<SecurityRequirementsOperationFilter>();
                options.OperationFilter<VersionOperationFilter>();
                options.DocumentFilter<VersionOperationFilter>();

                var xmlFile = $"{Assembly.GetEntryAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });
        }

        /// <summary>
        /// Other Swagger UI configuration.
        /// </summary>
        public static void UseConfiguredSwaggerInApp(this IApplicationBuilder application, IApiVersionDescriptionProvider provider)
        {
            application.UseSwagger();
            application.UseSwaggerUI(
                options =>
                {
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint(
                            $"/swagger/{description.GroupName}/swagger.json",
                            description.GroupName.ToUpperInvariant());
                    }
                });
        }
    }
}
