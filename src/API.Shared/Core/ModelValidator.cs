﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.Shared.Core
{
    /// <summary>
    /// The validation for models.
    /// </summary>
    public static class ModelValidator
    {
        private const string GreaterThenZeroMessage = "The field {0} must be greater than or equal to 0.";

        /// <summary>
        /// Greater the then zero.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="context">The context.</param>
        /// <returns>The result of int value validation.</returns>
        public static ValidationResult GreaterThenZero(int value, ValidationContext context)
        {
            return value > 0
                    ? ValidationResult.Success
                    : new ValidationResult(GreaterThenZeroMessage, new List<string> { context.MemberName });
        }

        /// <summary>
        /// Greater the then zero шт case not null.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="context">The context.</param>
        /// <returns>The result of int value validation.</returns>
        public static ValidationResult NullOrGreaterThenZero(int? value, ValidationContext context)
        {
            return value == null || value > 0
                    ? ValidationResult.Success
                    : new ValidationResult(GreaterThenZeroMessage, new List<string> { context.MemberName });
        }
    }
}
