﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace API.Shared.Core
{
    /// <summary>
    /// Filter for adding authorization headers to request.
    /// </summary>
    [ExcludeFromCodeCoverage]
    internal class SecurityRequirementsOperationFilter : IOperationFilter
    {
        private const string CorrelationIdDefinition = "CorrelationId";

        /// <inheritdoc />
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            operation.Security ??= new List<OpenApiSecurityRequirement>();

            var correlationId = new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = CorrelationIdDefinition
                }
            };
            operation.Security.Add(new OpenApiSecurityRequirement { [correlationId] = new List<string>() });
        }
    }
}
