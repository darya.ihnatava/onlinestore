﻿CREATE TABLE [dbo].[Product]
(
   [Id]                INT             NOT NULL PRIMARY KEY IDENTITY(1, 1),
   [Name]              NVARCHAR(50)    NOT NULL,
   [Description]       NVARCHAR(MAX)   NULL,
   [ImageUrl]          NVARCHAR(MAX)   NULL,
   [CategoryId]        INT             NOT NULL CONSTRAINT FK_Product$Category FOREIGN KEY REFERENCES [Category] ([Id]) ON DELETE CASCADE,
   [Price]             MONEY           NOT NULL,
   [Amount]            INT             NOT NULL DEFAULT(0)
)