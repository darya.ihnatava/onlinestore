﻿CREATE TABLE [dbo].[Category]
(
  [Id]                   INT            NOT NULL PRIMARY KEY IDENTITY(1, 1),
  [Name]                 NVARCHAR(50)   NOT NULL,
  [ImageUrl]             NVARCHAR(MAX)  NULL,
  [ParentCategoryId]     INT            NULL CONSTRAINT FK_Category$Category FOREIGN KEY REFERENCES [Category] ([Id])
)