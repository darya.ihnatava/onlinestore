﻿namespace Cart.Domain.Exceptions
{
    /// <summary>
    /// The error type.
    /// </summary>
    public enum ErrorType
    {
        /// <summary>
        /// Unknown error.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Instance not found.
        /// </summary>
        InstanceNotExist = 1,

        /// <summary>
        /// The collection item not found.
        /// </summary>
        CollectionItemNotExists = 2,

        /// <summary>
        /// The collection already contains specified item.
        /// </summary>
        CollectionItemAlreadyExists = 3,

        /// <summary>
        /// The bad request.
        /// </summary>
        BadRequest = 4,
    }
}
