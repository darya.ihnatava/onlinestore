﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Cart.Domain.Models
{
    /// <summary>
    /// The cart.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Cart
    {
        /// <summary>
        /// The identifier.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The cart items.
        /// </summary>
        public List<CartItem> Items { get; set; } = new List<CartItem>();
    }
}
