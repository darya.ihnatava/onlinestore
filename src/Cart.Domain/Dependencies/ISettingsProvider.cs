﻿namespace Cart.Domain.Dependencies
{
    /// <summary>
    /// The settings provider.
    /// </summary>
    public interface ISettingsProvider
    {
        /// <summary>
        /// Gets the database connection string.
        /// </summary>
        /// <returns>The connection string.</returns>
        string GetDatabaseConnectionString();
    }
}
