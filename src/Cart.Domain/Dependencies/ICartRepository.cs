﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Cart.Domain.Dependencies
{
    /// <summary>
    /// The cart repository.
    /// </summary>
    public interface ICartRepository
    {
        /// <summary>
        /// Gets cart by cart identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        /// <returns>The cart entity.</returns>
        Models.Cart GetById(Guid cartId);

        /// <summary>
        /// Creates a new cart.
        /// </summary>
        /// <param name="cart">The cart.</param>
        /// <returns>The cart entity.</returns>
        Models.Cart Create(Models.Cart cart);

        /// <summary>
        /// Removes cart by identifier.
        /// </summary>
        /// <param name="cartId">The cart identifier.</param>
        void Remove(Guid cartId);

        /// <summary>
        /// Updates cart.
        /// </summary>
        /// <param name="cart">The cart entity.</param>
        /// <returns>Teh updated cart.</returns>
        Models.Cart Update(Models.Cart cart);

        /// <summary>
        /// Get collection of carts by predicate.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>The collection of carts.</returns>
        List<Models.Cart> GetCartsByProductId(int productId);
    }
}
