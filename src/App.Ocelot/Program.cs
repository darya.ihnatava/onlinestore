using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace App.Ocelot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHost(args).Run();
        }

        public static IWebHost CreateHost(string[] args)
        {
            var builder = WebHost.CreateDefaultBuilder(args);

            builder.ConfigureServices(s => s.AddSingleton(builder))
                .ConfigureAppConfiguration(
                    ic => ic.AddJsonFile("ocelot.json"))
                .UseStartup<Startup>();
            var host = builder.Build();
            return host;
        }
    }
}
