﻿using System.Threading;
using System.Threading.Tasks;

namespace Kafka.Consumer
{
    /// <summary>
    /// THe consumer service.
    /// </summary>
    public class ConsumerService : Microsoft.Extensions.Hosting.BackgroundService
    {
        private readonly IKafkaConsumer _consumer;

        public ConsumerService(IKafkaConsumer consumer)
        {
            _consumer = consumer;
        }

        /// <inheritdoc />
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return _consumer.ExecuteAsync();
        }
    }
}
