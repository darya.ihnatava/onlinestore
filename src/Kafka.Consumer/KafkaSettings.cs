﻿using Kafka.Base.Core;

namespace Kafka.Consumer
{
    /// <summary>
    /// The kafka settings.
    /// </summary>
    public class KafkaSettings : KafkaSettingsBase
    {
        /// <summary>
        /// The group identifier.
        /// </summary>
        public string GroupId { get; set; }
    }
}
