﻿using Cart.BusinessLogic;
using Cart.Domain.Models;
using Confluent.Kafka;
using Kafka.Base;
using Kafka.Base.Core;
using Kafka.Base.Handlers;
using Microsoft.Extensions.Options;
using Serilog;
using System;
using System.Threading.Tasks;
using Serilog.Context;
using Serilog.Core;
using Serilog.Core.Enrichers;

namespace Kafka.Consumer
{
    /// <summary>
    /// The kafka consumer.
    /// </summary>
    public interface IKafkaConsumer
    {
        /// <summary>
        /// Execute consuming.
        /// </summary>
        Task ExecuteAsync();
    }

    /// <inheritdoc />
    public class KafkaConsumer : KafkaClientBase, IKafkaConsumer
    {
        private const string CorrelationIdDefinition = "CorrelationId";
        private readonly IKafkaClientProvider<Null, ChangeModel> _clientProvider;
        private readonly KafkaSettings _settings;
        private readonly ICartService _cartService;

        private ConsumerConfig _config;
        private ConsumerHandlers<Null, ChangeModel> _handlers;
        private IConsumer<Null, ChangeModel> _consumer;


        public KafkaConsumer(
            IKafkaClientProvider<Null, ChangeModel> clientProvider,
            ICartService cartService,
            IOptions<KafkaSettings> settings,
            ILogger logger)
            : base(logger, settings.Value)
        {
            _cartService = cartService;
            _settings = settings.Value;
            _clientProvider = clientProvider;
            InitialSetup();
        }

        private void InitialSetup()
        {
            _config = new ConsumerConfig
            {
                BootstrapServers = _settings.BootstrapServers,
                GroupId = _settings.GroupId,
                EnableAutoCommit = false,
                AutoOffsetReset = AutoOffsetReset.Earliest
            };
            _handlers = new ConsumerHandlers<Null, ChangeModel>
            {
                ErrorHandler = (_, error) => Logger.Error($"An error appeared on kafka side. Error details:{error}"),
                LogHandler = (_, message) => Logger.Debug(message.Message)
            };
            _consumer = _clientProvider.CreateConsumer(_config, _handlers);
        }

        /// <inheritdoc />
        public async Task ExecuteAsync()
        {
            await Task.Run(() => ConsumerLoop());
        }

        private async Task ConsumerLoop()
        {
            _consumer.Subscribe(_settings.Topic);

            while (true)
            {
                try
                {
                    var result = _consumer.Consume();
                    var change = result.Message.Value;
                    using (LogContext.Push(new ILogEventEnricher[] {new PropertyEnricher(CorrelationIdDefinition, change.CorrelationId)}))
                    {
                        _cartService.UpdateCartsByChangedProductData(change.ProductChange);
                    }

                    _consumer.Commit();
                    Logger.Information($"The product {change.ProductChange.Id} was updated in carts.");
                }
                catch (ConsumeException e)
                {
                    Logger.Error($"Consume error: {e.Error.Reason}");
                    if (e.Error.IsFatal)
                    {
                        break;
                    }
                }
                catch (Exception e)
                {
                    Logger.Error($"Unexpected error: {e}");
                }
            }
        }

        /// <inheritdoc />
        public override void Dispose()
        {
            _consumer.Dispose();
        }
    }
}