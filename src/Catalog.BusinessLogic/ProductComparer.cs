﻿using Catalog.Domain.Models;

namespace Catalog.BusinessLogic
{
    /// <summary>
    /// The roduct comparer
    /// </summary>
    internal static class ProductComparer
    {
        /// <summary>
        /// Compares two product objects on change existence.
        /// </summary>
        /// <param name="x">The product item.</param>
        /// <param name="y">The product item.</param>
        /// <returns>The result of comparison of two products.</returns>
        public static bool CompareTo(this Product x, Product y)
        {
            return x.Name.Equals(y.Name) &&
                   x.ImageUrl.Equals(y.ImageUrl) &&
                   x.Price.Equals(y.Price);
        }
    }
}
