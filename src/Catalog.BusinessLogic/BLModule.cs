﻿using Catalog.BusinessLogic.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Catalog.BusinessLogic
{
    /// <summary>
    /// The registration on business logic services.
    /// </summary>
    public static class BLModule
    {
        /// <summary>
        /// The method on business logic services.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void AddServiceDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICategoryService, CategoryService>();
            serviceCollection.AddTransient<IProductService, ProductService>();
        }
    }
}
