﻿using Catalog.Domain.Dependencies;
using Catalog.Domain.Exceptons;
using Catalog.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.BusinessLogic.Services
{
    public interface IProductService
    {
        /// <summary>
        /// Gets the filtered count.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <returns>The count of the filtered records.</returns>
        Task<int> GetFilteredCount(int? categoryId);

        /// <summary>
        /// Gets all by category identifier with pagination.
        /// </summary>
        /// <param name="categoryId">The category identifier.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <param name="itemsOnPageCount">The number of items on the page.</param>
        /// <returns>The collection of products by category and paginated.</returns>
        Task<List<Product>> GetAllByCategoryIdWithPagination(int? categoryId, int itemsOnPageCount, int pageNumber);

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>The collection of items.</returns>
        Task<List<Product>> GetAll();

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The item.</returns>
        Task<Product> GetById(int id);

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        Task<Product> Add(Product item);

        /// <summary>
        /// Updates the specified item.
        /// </summary>
        /// <param name="id">The item identifier.</param>
        /// <param name="item">The item.</param>
        Task<Product> Update(Product item);

        /// <summary>
        /// Deletes the specified item.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        Task Delete(int itemId);

        /// <summary>
        /// Get tha change of product.
        /// </summary>
        /// <param name="item">The new item.</param>
        /// <returns>Product change model.</returns>
        Task<Domain.Models.ProductChangeModel> GetProductChange(Product item);
    }

    /// <inheritdoc />
    internal class ProductService : ServiceBase<Product>, IProductService
    {
        private readonly IRepository<Category> _categoryRepository;

        public ProductService(
            IRepository<Product> repository,
            IRepository<Category> categoryRepository)
            : base(repository)
        {
            _categoryRepository = categoryRepository;
        }

        /// <inheritdoc />
        public async Task<int> GetFilteredCount(int? categoryId)
        {
            if (categoryId != null)
            {
                await ValidateCategory(categoryId.Value);
            }

            var count = Repository.GetAll().Count(x => categoryId == null || x.CategoryId == categoryId);
            return count;
        }

        /// <inheritdoc />
        public async Task<List<Product>> GetAllByCategoryIdWithPagination(int? categoryId, int countOnPage, int pageNumber)
        {
            if (categoryId != null)
            {
                await ValidateCategory(categoryId.Value);
            }

            var result = Repository.GetAll()
                                   .Where(x => categoryId == null || x.CategoryId == categoryId)
                                   .Skip(countOnPage * (pageNumber - 1))
                                   .Take(countOnPage)
                                   .ToList();
            return result;
        }

        /// <inheritdoc />
        public override async Task<Product> Add(Product item)
        {
            await ValidateCategory(item.CategoryId);
            return await base.Add(item);
        }

        /// <inheritdoc />
        public async Task<Product> Update(Product item)
        {
            await ValidateCategory(item.CategoryId);
            await base.Update(item);
            return item;
        }

        /// <inheritdoc />
        public async Task<Domain.Models.ProductChangeModel> GetProductChange(Product item)
        {
            var oldValue = await GetById(item.Id);
            var change = CreateProductChange(oldValue, item);
            return change;
        }

        /// <summary>
        /// Verify if record exist. If not - throw specific exception.
        /// </summary>
        /// <param name="categoryId">The category to check.</param>
        private async Task ValidateCategory(int categoryId)
        {
            var isExists = await _categoryRepository.GetById(categoryId);
            if (isExists == null)
            {
                throw new ServiceException($"Category {categoryId} not found.", ErrorType.LinkedInstanceNotExist);
            }
        }

        private Domain.Models.ProductChangeModel CreateProductChange(Product oldValue, Product newValue)
        {
            if (oldValue.CompareTo(newValue))
            {
                return null;
            }
            var change = new Domain.Models.ProductChangeModel
            {
                Id = newValue.Id,
                Name = newValue.Name,
                ImageUrl = newValue.ImageUrl,
                Price = newValue.Price,
                Quantity = newValue.Amount
            };
            return change;
        }
    }
}
