﻿using Catalog.Domain;
using Catalog.Domain.Dependencies;
using Catalog.Domain.Exceptons;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.BusinessLogic.Services
{
    /// <inheritdoc />
    internal class ServiceBase<T>
        where T : IModel
    {
        protected ServiceBase(IRepository<T> repository)
        {
            Repository = repository;
        }

        protected IRepository<T> Repository { get; }

        /// <inheritdoc />
        public async Task<List<T>> GetAll()
        {
            return await Task.Run(() => Repository.GetAll().ToList());
        }

        /// <inheritdoc />
        public async Task<T> GetById(int id)
        {
            var item = await EnsureRecordExistence(id);
            return item;
        }

        /// <inheritdoc />
        public async virtual Task<T> Add(T item)
        {
            var addedItem = await Repository.Add(item);
            return addedItem;
        }

        /// <inheritdoc />
        public async virtual Task<T> Update(T item)
        {
            await EnsureRecordExistence(item.Id);
            var updatedItem = await Repository.Update(item);
            return updatedItem;
        }

        /// <inheritdoc />
        public async virtual Task Delete(int itemId)
        {
            await EnsureRecordExistence(itemId);
            await Repository.Delete(itemId);
        }

        /// <summary>
        /// Verify if record exist. If not - throw specific exception.
        /// </summary>
        /// <param name="record">Record to check.</param>
        /// <param name="id">Record identifier.</param>
        protected async Task<T> EnsureRecordExistence(int id, ErrorType errorType = ErrorType.InstanceNotExist)
        {
            var item = await Repository.GetById(id);
            if (item == null)
            {
                throw new ServiceException($"{typeof(T).Name} {id} not found.", errorType);
            }

            return item;
        }
    }
}
