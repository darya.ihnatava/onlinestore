﻿using Catalog.Domain.Dependencies;
using Catalog.Domain.Exceptons;
using Catalog.Domain.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.BusinessLogic.Services
{
    public interface ICategoryService
    {
        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>The collection of items.</returns>
        Task<List<Category>> GetAll();

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The item.</returns>
        Task<Category> GetById(int id);

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        Task<Category> Add(Category item);

        /// <summary>
        /// Updates the specified item.
        /// </summary>
        /// <param name="id">The item identifier.</param>
        /// <param name="item">The item.</param>
        Task<Category> Update(Category item);

        /// <summary>
        /// Deletes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        Task Delete(int itemId);
    }

    /// <inheritdoc />
    internal class CategoryService : ServiceBase<Category>, ICategoryService
    {
        public CategoryService(
            IRepository<Category> repository)
            : base(repository)
        {
        }

        /// <inheritdoc />
        public override async Task<Category> Add(Category item)
        {
            await ValidateParentCategory(item.ParentCategoryId);
            return await base.Add(item);
        }

        /// <inheritdoc />
        public override async Task<Category> Update(Category item)
        {
            await ValidateParentCategory(item.ParentCategoryId);
            return await base.Update(item);
        }

        /// <inheritdoc />
        public override Task Delete(int itemId)
        {
            EnsureNoLinkedChildCategories(itemId);
            return base.Delete(itemId);
        }

        private async Task ValidateParentCategory(int? parentCategoryId)
        {
            if (parentCategoryId == null)
            {
                return;
            }

            await EnsureRecordExistence(parentCategoryId.Value, ErrorType.LinkedInstanceNotExist);
        }

        private void EnsureNoLinkedChildCategories(int itemId)
        {
            var linkedCategories = Repository.GetAll().Where(x => x.ParentCategoryId == itemId);
            var isExists = linkedCategories.Any();
            if (isExists)
            {
                throw new ServiceException($"There are categories that has the category as linked to.", ErrorType.InstanceHasLinkedObjects);
            }
        }
    }
}
