﻿using Catalog.Domain;
using Catalog.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Catalog.DataAccess
{
    /// <summary>
    /// The context.
    /// </summary>
    /// <seealso cref="DbContext" />
    public class Context : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Context"/> class.
        /// </summary>
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// The users.
        /// </summary>
        public DbSet<Category> Category { get; set; }

        /// <summary>
        /// The users.
        /// </summary>
        public DbSet<Product> Product { get; set; }


        /// <summary>
        /// On model creating method.
        /// </summary>
        /// <param name="modelBuilder">Model builder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasKey(x => new { x.Id });
            modelBuilder.Entity<Product>().HasKey(x => new { x.Id });
        }
    }
}
