﻿using System.Linq;
using System.Threading.Tasks;
using Catalog.Domain;
using Catalog.Domain.Dependencies;
using Microsoft.EntityFrameworkCore;

namespace Catalog.DataAccess
{
    /// <inheritdoc />
    internal class Repository<T> : IRepository<T> 
        where T: class, IModel
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;

        public Repository(Context context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }

        /// <inheritdoc />
        public IQueryable<T> GetAll()
        {
            return _dbSet.AsNoTracking();
        }

        /// <inheritdoc />
        public async Task<T> GetById(int id)
        {
            return await _dbSet.FindAsync(id);
        }

        /// <inheritdoc />
        public async Task<T> Add(T item)
        {
            _dbSet.Add(item);
            await _context.SaveChangesAsync();
            return item;
        }

        /// <inheritdoc />
        public async Task<T> Update(T item)
        {
            var toUpdate = await GetById(item.Id);
            _context.Entry(toUpdate).CurrentValues.SetValues(item);
            _context.Entry(toUpdate).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return item;
        }

        /// <inheritdoc />
        public async Task Delete(int itemId)
        {
            var item = await GetById(itemId);
            _dbSet.Remove(item);
            await _context.SaveChangesAsync();
        }
    }
}
