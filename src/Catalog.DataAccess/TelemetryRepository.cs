﻿using Catalog.Domain;
using Catalog.Domain.Dependencies;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.DataAccess
{
    /// <summary>
    /// The telemetry repository.
    /// </summary>
    /// <typeparam name="T">The type.</typeparam>
    public class TelemetryRepository<T> : IRepository<T>
        where T : IModel
    {
        private const string TelemetryKey = "Database";
        private TelemetryClient _telemetryClient;
        private readonly IRepository<T> _repository;

        public TelemetryRepository(
            IRepository<T> repository,
            TelemetryClient telemetryClient)
        {
            _repository = repository;
            _telemetryClient = telemetryClient;
        }

        public IQueryable<T> GetAll()
        {
            var data = $"{typeof(T).Name} {nameof(GetAll)}";
            using (var operation = _telemetryClient.StartOperation<DependencyTelemetry>(TelemetryKey))
            {
                operation.Telemetry.Data = data;
                var result = _repository.GetAll();
                operation.Telemetry.Success = true;
                _telemetryClient.StopOperation(operation);
                return result;
            }
        }

        public async Task Delete(int itemId)
        {
            var data = $"{typeof(T).Name} {nameof(Delete)}";
            using (var operation = _telemetryClient.StartOperation<DependencyTelemetry>(TelemetryKey))
            {
                operation.Telemetry.Data = data;
                try
                {
                    await _repository.Delete(itemId);
                }
                catch (Exception)
                {
                    operation.Telemetry.Success = false;
                    throw;
                }

                operation.Telemetry.Success = true;
                _telemetryClient.StopOperation(operation);
            }
        }

        public async Task<T> Add(T item)
        {
            return await PerformInTelemetry(_repository.Add(item), nameof(Add));
        }

        public async Task<T> GetById(int id)
        {
            return await PerformInTelemetry(_repository.GetById(id), nameof(GetById));
        }

        public async Task<T> Update(T item)
        {
            return await PerformInTelemetry(_repository.Update(item), nameof(Update));
        }

        private async Task<T> PerformInTelemetry(Task<T> task, string name)
        {
            var data = $"{typeof(T).Name} {name}";
            using (var operation = _telemetryClient.StartOperation<DependencyTelemetry>(TelemetryKey))
            {
                var result = default(T);
                operation.Telemetry.Data = data;
                try
                {
                    result = await task;
                }
                catch (Exception)
                {
                    operation.Telemetry.Success = false;
                    throw;
                }

                operation.Telemetry.Success = true;
                _telemetryClient.StopOperation(operation);
                return result;
            }
        }
    }
}
