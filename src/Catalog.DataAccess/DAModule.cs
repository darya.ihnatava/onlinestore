﻿using Catalog.Domain.Dependencies;
using Catalog.Domain.Models;

using Microsoft.Extensions.DependencyInjection;

namespace Catalog.DataAccess
{
    /// <summary>
    /// The registration on business logic services.
    /// </summary>
    public static class DAModule
    {
        /// <summary>
        /// The method on business logic services.
        /// </summary>
        /// <param name="serviceCollection">The service collection.</param>
        public static void AddRepositoryDependencies(this IServiceCollection serviceCollection, bool isTelemetryClientEnabled = false)
        {
            serviceCollection.AddTransient<IRepository<Category>, Repository<Category>>();
            serviceCollection.AddTransient<IRepository<Product>, Repository<Product>>();
            if (isTelemetryClientEnabled)
            {
                serviceCollection.Decorate(typeof(IRepository<>), typeof(TelemetryRepository<>));
            }
        }
    }
}
