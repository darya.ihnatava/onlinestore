﻿using API.Shared.Core;
using System.ComponentModel.DataAnnotations;

namespace Catalog.API.Models
{
    /// <summary>
    /// The product information.
    /// </summary>
    public class ProductInfo
    {
        /// <summary>
        /// The name.
        /// </summary>
        [MinLength(5)]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// The description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        [Url]
        public string ImageUrl { get; set; }

        /// <summary>
        /// The category identifier.
        /// </summary>
        [CustomValidation(typeof(ModelValidator), nameof(ModelValidator.GreaterThenZero))]
        public int CategoryId { get; set; }

        /// <summary>
        /// The price.
        /// </summary>
        [CustomValidation(typeof(ModelValidator), nameof(ModelValidator.GreaterThenZero))]
        public decimal Price { get; set; }

        /// <summary>
        /// The amount.
        /// </summary>
        [CustomValidation(typeof(ModelValidator), nameof(ModelValidator.GreaterThenZero))]
        public int Amount { get; set; }
    }
}
