﻿using API.Shared.Core;
using System.ComponentModel.DataAnnotations;

namespace Catalog.API.Models
{
    /// <summary>
    /// The category information.
    /// </summary>
    public class CategoryInfo
    {
        /// <summary>
        /// The name.
        /// </summary>
        [MaxLength(50)]
        [MinLength(5)]
        public string Name { get; set; }

        /// <summary>
        /// The image URL.
        /// </summary>
        [Url]
        public string ImageUrl { get; set; }

        /// <summary>
        /// The parent category identifier.
        /// </summary>
        [CustomValidation(typeof(ModelValidator), nameof(ModelValidator.NullOrGreaterThenZero))]
        public int? ParentCategoryId { get; set; }
    }
}
