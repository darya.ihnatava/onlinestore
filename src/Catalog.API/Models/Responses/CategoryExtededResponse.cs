﻿using Catalog.API.Models.Requests;
using System.Collections.Generic;

namespace Catalog.API.Models.Responses
{
    /// <summary>
    /// The extended category response.
    /// </summary>
    /// <seealso cref="CategoryInfo" />
    public class CategoryExtededResponse : CategoryInfo, IExtended
    {
        /// <summary>
        /// The links.
        /// </summary>
        public List<Link> Links { get; set; }
    }
}
