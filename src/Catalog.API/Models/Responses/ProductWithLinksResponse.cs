﻿using System.Collections.Generic;

namespace Catalog.API.Models.Responses
{
    /// <summary>
    /// The response with product information.
    /// </summary>
    public class ProductsWithLinksResponse
    {
        /// <summary>
        /// The products.
        /// </summary>
        public List<ProductResponse> Products { get; set; } = new List<ProductResponse>();

        /// <summary>
        /// The links next.
        /// </summary>
        public List<string> LinksNext { get; set; } = new List<string>();
    }
}
