﻿namespace Catalog.API.Models.Responses
{
    /// <summary>
    /// The response with category information.
    /// </summary>
    public class CategoryResponse : CategoryInfo
    {
        /// <summary>
        /// The category identifier.
        /// </summary>
        public int Id { get; set; }
    }
}
