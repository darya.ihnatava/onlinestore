﻿namespace Catalog.API.Models.Responses
{
    /// <summary>
    /// The response with product information.
    /// </summary>
    public class ProductResponse : ProductInfo
    {
        /// <summary>
        /// The product identifier.
        /// </summary>
        public int Id { get; set; }
    }
}
