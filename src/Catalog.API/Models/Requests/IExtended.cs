﻿using System.Collections.Generic;

namespace Catalog.API.Models.Requests
{
    /// <summary>
    /// The extended interface.
    /// </summary>
    public interface IExtended
    {
        /// <summary>
        /// The links.
        /// </summary>
        public List<Link> Links { get; set; }
    }
}
