﻿using System.Security.Policy;

namespace Catalog.API.Models.Requests
{
    /// <summary>
    /// The link repsentation.
    /// </summary>
    public class Link
    {
        /// <summary>
        /// The description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The URL.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// The method.
        /// </summary>
        public string Method { get; set; }
    }
}
