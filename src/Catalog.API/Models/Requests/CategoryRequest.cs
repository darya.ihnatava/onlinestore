﻿namespace Catalog.API.Models.Requests
{
    /// <summary>
    /// The request with category data.
    /// </summary>
    public class CategoryRequest : CategoryInfo
    {
    }
}
