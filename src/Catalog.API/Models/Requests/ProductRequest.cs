﻿namespace Catalog.API.Models.Requests
{
    /// <summary>
    /// The request with product data.
    /// </summary>
    public class ProductRequest : ProductInfo
    {
    }
}
