﻿using Identity.Client;
using System.Threading.Tasks;
using Catalog.API.Core;

namespace Catalog.API.Auth
{
    /// <summary>
    /// The permissions check service.
    /// </summary>
    public interface IPermissionCheckService
    {
        /// <summary>
        /// HAs access to resource check.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="path">The path.</param>
        /// <param name="correlationId">The correlation id.</param>
        /// <returns></returns>
        Task<bool> HasAccess(string token, string path, string correlationId);
    }

    /// <inheritdoc />
    public class PermissionCheckService : IPermissionCheckService
    {
        private readonly IIdentityClientFactory _identityFactory;

        /// <summary>
        /// Initializes the permission check service.
        /// </summary>
        /// <param name="identityFactory">The identity api client factory.</param>
        public PermissionCheckService(IIdentityClientFactory identityFactory)
        {
            _identityFactory = identityFactory;
        }

        /// <inheritdoc />
        public async Task<bool> HasAccess(string token, string path, string correlationId)
        {
            var client = _identityFactory.CreateIdentityClient(correlationId);
            var hasAccess = await client.VerifyUserToken(token, path);
            return hasAccess;
        }
    }
}
