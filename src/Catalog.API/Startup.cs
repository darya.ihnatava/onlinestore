using API.Shared.Swagger;
using AutoMapper;
using Catalog.API.Auth;
using Catalog.API.Core;
using Catalog.API.Mappers;
using Catalog.API.Middlewares;
using Catalog.BusinessLogic;
using Catalog.DataAccess;
using Catalog.Domain.Dependencies;
using Identity.Client;
using Kafka.Base;
using Kafka.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestEase;
using Serilog;
using Serilog.Events;
using System;
using System.Net.Http;
using API.Shared.Core;
using Microsoft.ApplicationInsights.DependencyCollector;

namespace Catalog.API
{
    /// <summary>
    /// The main configuration of the api.
    /// </summary>
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private const string ApiDescription = "Catalog API is a system that provides items by categories.";
        private const string ApiTitle = "Catalog API";
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            InitializeLogger(services);
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });
            services.AddSingleton<IMapper>(mapperConfig.CreateMapper());
            services.Configure<Settings>(_configuration);
            services.Configure<KafkaSettings>(_configuration.GetSection("Kafka"));
            services.AddTransient<ISettingsProvider, SettingsProvider>();
            var connectionString = services.BuildServiceProvider().GetService<ISettingsProvider>().GetDatabaseConnectionString();
            services.AddSingleton<IIdentityClientFactory, IdentityClientFactory>();
            services.AddTransient<IIdentityClient, IdentityClient>();
            services.AddDbContext<Context>(options => options.UseSqlServer(connectionString));
            services.AddServiceDependencies();
            services.AddRepositoryDependencies(true);

            services.AddTransient<IProducer, KafkaProducer>();

            services.AddSingleton(typeof(IKafkaClientProvider<,>), typeof(KafkaClientProvider<,>));

            services.AddControllers();
            services.AddSwaggerConfiguration(ApiTitle, ApiDescription);
            services.AddVersionedApiExplorer(opt => opt.GroupNameFormat = "'v'VVV");

            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true; 
                config.ApiVersionReader = ApiVersionReader.Combine(new HeaderApiVersionReader("x-version"), new UrlSegmentApiVersionReader());
            });

            services.AddTransient<IPermissionCheckService, PermissionCheckService>();
            services.AddApplicationInsightsTelemetry();
            services.AddMvcCore(options => options.Filters.Add<ApiExceptionFilterAttribute>())
                    .AddApiExplorer()
                    .AddDataAnnotations();
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="apiVersionDescriptionProvider">The API version description provider.</param>
        /// <param name="serviceProvider">The service provider.</param>
        public void Configure(
            IApplicationBuilder app,
            IApiVersionDescriptionProvider apiVersionDescriptionProvider,
            IServiceProvider serviceProvider)
        {
            var loggerFactory = serviceProvider.GetService<ILoggerFactory>();
            loggerFactory.AddSerilog();

            app.UseDeveloperExceptionPage();
            app.UseConfiguredSwaggerInApp(apiVersionDescriptionProvider);
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseMiddleware<CorrelationIdMiddleware>();
            app.UseMiddleware<AuthMiddleware>();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void InitializeLogger(IServiceCollection services)
        {
            if (Log.Logger == null)
            {
                const string exceptionMessage = "Logger instance was not initialized before registering into DI system";
                throw new InvalidOperationException(exceptionMessage);
            }

            Log.Logger = new LoggerConfiguration()
                    .ReadFrom
                    .Configuration(_configuration, "Serilog")
                    .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Warning)
                    .CreateLogger();
            services.AddSingleton(Log.Logger);
        }
    }
}
