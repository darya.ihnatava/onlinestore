﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using Catalog.API.ErrorHandling;
using Catalog.Domain.Exceptons;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;
using ExceptionFilterAttribute = Microsoft.AspNetCore.Mvc.Filters.ExceptionFilterAttribute;

namespace Catalog.API.Core
{
    /// <inheritdoc />
    [ExcludeFromCodeCoverage]
    public sealed class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger _logger;
        private readonly TelemetryClient _telemetryClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="ApiExceptionFilterAttribute"/> class.
        /// </summary>
        public ApiExceptionFilterAttribute(
            ILogger logger,
            TelemetryClient telemetryClient)
        {
            _logger = logger;
            _telemetryClient = telemetryClient;
        }

        /// <inheritdoc />
        public override void OnException(ExceptionContext context)
        {
            ApiError error;
            bool isValidationException = false;
            if (context.Exception is ServiceException ex)
            {
                isValidationException = true;
                HttpStatusCode httpCode;
                switch (ex.StatusCode)
                {
                    case ErrorType.InstanceNotExist:
                        httpCode = HttpStatusCode.NotFound;
                        break;
                    case ErrorType.LinkedInstanceNotExist:
                        httpCode = HttpStatusCode.BadRequest;
                        break;
                    case ErrorType.Forbidden:
                        httpCode = HttpStatusCode.Forbidden;
                        break;
                    default:
                        httpCode = HttpStatusCode.InternalServerError;
                        isValidationException = false;
                        break;
                }

                context.HttpContext.Response.StatusCode = (int)httpCode;
                error = new ApiError(context.Exception, ex.StatusCode);
                _telemetryClient.TrackException(context.Exception, new Dictionary<string, string> { { "HttpStatusCode", httpCode.ToString() } });
            }
            else
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                error = new ApiError(context.Exception, ErrorType.Unknown);
                _telemetryClient.TrackException(context.Exception, new Dictionary<string, string> { { "HttpStatusCode", ErrorType.Unknown.ToString() }});
            }

            if (isValidationException)
            {
                _logger.Warning(error.ToString());
            }
            else
            {
                _logger.Error(error.ToString());
            }

            context.Result = new ObjectResult(error);
            base.OnException(context);
        }
    }}
