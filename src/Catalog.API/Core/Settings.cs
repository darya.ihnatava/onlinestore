﻿namespace Catalog.API.Core
{
    /// <summary>
    /// The application settings.
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// The connection string.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// The items on the page count.
        /// </summary>
        public int ItemsOnThePageCount { get; set; }

        /// <summary>
        /// The identity service assress.
        /// </summary>
        public string IdentityServiceAddress { get; set; }
    }
}
