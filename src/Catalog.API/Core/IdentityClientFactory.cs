﻿using System;
using System.Net.Http;
using Identity.Client;
using Microsoft.Extensions.Options;
using RestEase;

namespace Catalog.API.Core
{
    /// <summary>
    /// THe identity client interface.
    /// </summary>
    public interface IIdentityClientFactory
    {
        /// <summary>
        /// Create Identity client.
        /// </summary>
        /// <param name="correlationId">The correlationId.</param>
        /// <returns>THe identity client.</returns>
        IIdentityClient CreateIdentityClient(string correlationId);
    }

    /// <inheritdoc />
    public class IdentityClientFactory : IIdentityClientFactory
    {
        private readonly string _identityServiceAddress;

        /// <summary>
        /// Creates instance.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public IdentityClientFactory(IOptions<Settings> settings)
        {
            _identityServiceAddress = settings.Value.IdentityServiceAddress;
        }

        /// <inheritdoc />
        public IIdentityClient CreateIdentityClient(string correlationId)
        {
            var api = CreateIdentityApi(correlationId);
            api.CorrelationId = correlationId;
            return new IdentityClient(api);

        }
        
        private IIdentityApi CreateIdentityApi(string correlationId)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(_identityServiceAddress)
            };

            var client = new RestClient(httpClient);
            var api = client.For<IIdentityApi>();
            api.CorrelationId = correlationId;

            return api;
        }
    }
}
