﻿using AutoMapper;
using Catalog.API.ErrorHandling;
using Catalog.API.Models.Requests;
using Catalog.API.Models.Responses;
using Catalog.BusinessLogic.Services;
using Catalog.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Catalog.API.Controllers
{
    /// <summary>
    /// The category controller.
    /// </summary>
    [ApiController]
    [Route("api/v{version:apiVersion}/categories")]
    [ApiVersion("1.0", Deprecated = true)]
    [ApiVersion("2.0")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryController"/> class.
        /// </summary>
        /// <param name="categoryService">The category service.</param>
        /// <param name="mapper">The mapper.</param>
        public CategoryController(
            ICategoryService categoryService,
            IMapper mapper)
        {
            _mapper = mapper;
            _categoryService = categoryService;
        }

        /// <summary>
        /// Get all categories.
        /// </summary>
        /// <response code="200">The category exists and was successfully returned.</response>
        /// <returns>The collection of categories.</returns>
        [HttpGet]
        [Route("")]
        [ProducesResponseType(typeof(List<CategoryResponse>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var categories = await _categoryService.GetAll();
            var responseCategories = _mapper.Map<List<CategoryResponse>>(categories);
            return Ok(responseCategories);
        }

        /// <summary>
        /// Gets the category by identifier.
        /// </summary>
        /// <param name="id">The identifier of category.</param>
        /// <response code="200">The category exists and was successfully returned.</response>
        /// <response code="404">The category not exists.</response>
        /// <returns>The category.</returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(CategoryResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(int id)
        {
            var category = await _categoryService.GetById(id);
            var responseCategory = _mapper.Map<CategoryResponse>(category);
            return Ok(responseCategory);
        }

        /// <summary>
        /// Adds the category by specified request.
        /// </summary>
        /// <param name="request">The request with category data.</param>
        /// <returns>The added category.</returns>
        [HttpPost]
        [Route("")]
        [ProducesResponseType(typeof(CategoryResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Add([FromBody] CategoryRequest request)
        {
            var requestCategory = _mapper.Map<Category>(request);
            var category = await _categoryService.Add(requestCategory);
            var responseCategory = _mapper.Map<CategoryResponse>(category);
            return StatusCode(StatusCodes.Status201Created, responseCategory);
        }

        /// <summary>
        /// Updates the category by specified identifier.
        /// </summary>
        /// <param name="id">The identifier of category.</param>
        /// <param name="request">The request.</param>
        /// <response code="200">The category was updated.</response>
        /// <response code="400">The bad request. Wrong setup of new category data.</response>
        /// <response code="404">The category not exists.</response>
        /// <returns>The updated category</returns>
        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(CategoryResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update(int id, [FromBody] CategoryRequest request)
        {
            var requestCategory = _mapper.Map<Category>(request);
            requestCategory.Id = id;
            var category = await _categoryService.Update(requestCategory);
            var responseCategory = _mapper.Map<CategoryResponse>(category);
            return Ok(responseCategory);
        }

        /// <summary>
        /// Deletes the category by specified identifier.
        /// </summary>
        /// <param name="id">The identifier of category.</param>
        /// <response code="200">The category was removed.</response>
        /// <response code="404">The category does not exists.</response>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            await _categoryService.Delete(id);
            return Ok();
        }
    }
}
