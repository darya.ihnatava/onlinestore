﻿using AutoMapper;
using Catalog.API.Core;
using Catalog.API.ErrorHandling;
using Catalog.API.Models.Requests;
using Catalog.API.Models.Responses;
using Catalog.BusinessLogic.Services;
using Catalog.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kafka.Client;

namespace Catalog.API.Controllers
{
    /// <summary>
    /// The product controller.
    /// </summary>
    /// <seealso cref="ControllerBase" />
    [ApiController]
    [Route("api/v{version:apiVersion}/products")]
    [ApiVersion("1.0", Deprecated = true)]
    [ApiVersion("2.0")]
    public class ProductController : ControllerBase
    {
        private const string CorrelationIdHeader = "x-correlation-id";
        private readonly IProductService _productService;
        private readonly IOptions<Settings> _settings;
        private readonly IProducer _producer;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductController"/> class.
        /// </summary>
        /// <param name="productService">The product service.</param>
        /// <param name="mapper">The mapper.</param>
        /// <param name="settings">The settings.</param>
        public ProductController(
            IProductService productService,
            IOptions<Settings> settings,
            IProducer producer,
            IMapper mapper)
        {
            _mapper = mapper;
            _producer = producer;
            _settings = settings;
            _productService = productService;
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>The collection of products.</returns>
        [HttpGet]
        [Route("")]
        [ApiVersion("1.0")]
        [ProducesResponseType(typeof(List<ProductResponse>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
        {
            var products = await _productService.GetAll();
            var responseProducts = _mapper.Map<List<ProductResponse>>(products);
            return Ok(responseProducts);
        }


        /// <summary>
        /// Get all categories.
        /// </summary>
        /// <returns>The collection of categories.</returns>
        [HttpGet]
        [Route("", Name = nameof(GetAll))]
        [ApiVersion("2.0")]
        [ProducesResponseType(typeof(ProductsWithLinksResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetAll([FromQuery] int? categoryId, [FromQuery] int pageNumber)
        {
            if (pageNumber < 1)
            {
                return BadRequest("The page number must be greater then 0");
            }

            var itemsOnThePage = _settings.Value.ItemsOnThePageCount;
            var count = await _productService.GetFilteredCount(categoryId);
            var pagesCount = (int)Math.Ceiling((decimal)count / itemsOnThePage);
            if (pagesCount < pageNumber)
            {
                return Ok(new ProductsWithLinksResponse());
            }

            var products = await _productService.GetAllByCategoryIdWithPagination(categoryId, itemsOnThePage, pageNumber);
            var responseProducts = _mapper.Map<List<ProductResponse>>(products);
            List<string> links = GetNextLinls(categoryId, pageNumber, pagesCount);

            var response = new ProductsWithLinksResponse
            {
                Products = responseProducts,
                LinksNext = links
            };
            return Ok(response);
        }

        /// <summary>
        /// Gets the product by identifier.
        /// </summary>
        /// <param name="id">The identifier of product.</param>
        /// <returns>The product.</returns>
        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(typeof(ProductResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(int id)
        {
            var product = await _productService.GetById(id);
            var responseProduct = _mapper.Map<ProductResponse>(product);
            return Ok(responseProduct);
        }

        /// <summary>
        /// Gets the product properties.
        /// </summary>
        /// <param name="id">The identifier of product.</param>
        /// <returns>The product properties.</returns>
        [HttpGet]
        [Route("properties/{id}")]
        [ProducesResponseType(typeof(Dictionary<string, string>), StatusCodes.Status200OK)]
        public IActionResult GetProperties(int id)
        {
            var dictionary = new Dictionary<string, string>
            {
                { "Brand", "Samsung"},
                { "Model", "Note20"},
                { "Android", "10.0"},
            };
            return Ok(dictionary);
        }

        /// <summary>
        /// Adds the product by specified request.
        /// </summary>
        /// <param name="request">The request with product data.</param>
        /// <returns>The added product.</returns>
        [HttpPost]
        [Route("")]
        [ProducesResponseType(typeof(ProductResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Add([FromBody] ProductRequest request)
        {
            var product = _mapper.Map<Product>(request);
            var addedProduct = await _productService.Add(product);
            var productResponse = _mapper.Map<ProductResponse>(addedProduct);
            return StatusCode(StatusCodes.Status201Created, productResponse);
        }

        /// <summary>
        /// Updates the product by specified identifier.
        /// </summary>
        /// <param name="id">The identifierof product.</param>
        /// <param name="request">The request with product data.</param>
        /// <param name="correlationId">The correlation id.</param>
        /// <returns>The updated product.</returns>
        [HttpPut]
        [Route("{id}")]
        [ProducesResponseType(typeof(ProductResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update(int id, [FromBody] ProductRequest request, [FromHeader(Name = CorrelationIdHeader)] string correlationId)
        {
            var product = _mapper.Map<Product>(request);
            product.Id = id;

            var productChange = await _productService.GetProductChange(product);
            var updatedProduct = await _productService.Update(product);
            if (productChange != null)
            {
                await _producer.ProduceAsync(productChange, correlationId);
            }

            var productResponse = _mapper.Map<ProductResponse>(updatedProduct);
            return Ok(productResponse);
        }

        /// <summary>
        /// Deletes the product by specified identifier.
        /// </summary>
        /// <param name="id">The identifier of product.</param>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiError), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            await _productService.Delete(id);
            return Ok();
        }
         
        private List<string> GetNextLinls(int? categoryId, int pageNumber, int pagesCount)
        {
            List<string> links = new List<string>();
            for (int i = pageNumber + 1; i <= pagesCount; i++)
            {
                var link = Url.RouteUrl(nameof(GetAll), new { categoryId = categoryId, pageNumber = i });
                links.Add(link);
            }

            return links;
        }
    }
}
