﻿using AutoMapper;
using Catalog.API.Models.Requests;
using Catalog.API.Models.Responses;
using Catalog.Domain.Models;

namespace Catalog.API.Mappers
{
    /// <summary>
    /// The mapping profile.
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes mapping profiles.
        /// </summary>
        public MappingProfile()
        {
            CreateMap<CategoryRequest, Category>();
            CreateMap<Category, CategoryResponse>();

            CreateMap<ProductRequest, Product>();
            CreateMap<Product, ProductResponse>();
        }
    }
}
