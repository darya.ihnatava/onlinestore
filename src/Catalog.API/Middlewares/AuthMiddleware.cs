﻿using Catalog.API.Auth;
using Catalog.API.Core;
using Catalog.API.ErrorHandling;
using Catalog.Domain.Exceptons;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System.Net;
using System.Threading.Tasks;

namespace Catalog.API.Middlewares
{
    /// <summary>
    /// The authorization middleware.
    /// </summary>
    public class AuthMiddleware
    {
        private const string CorrelationIdHeader = "x-correlation-id";
        private const string ControllerRouteKey = "controller";
        private const string ActionRouteKey = "action";
        private const string BearerTokenPrefix = "Bearer ";
        private readonly RequestDelegate _next;
        private readonly IPermissionCheckService _permissionCheckService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthMiddleware"/> class.
        /// </summary>
        public AuthMiddleware(
            RequestDelegate next,
            IPermissionCheckService permissionCheckService)
        {
            _next = next;
            _permissionCheckService = permissionCheckService;
        }

        /// <summary>
        /// Invoke the middleware.
        /// </summary>
        public async Task Invoke(HttpContext context)
        {
            if (!context.Request.Headers.Keys.Contains(HeaderNames.Authorization))
            {
                var tokenMissingError = new ApiError(description: "Auth token is missing", code: ErrorType.NotAuthorized);
                await context.WriteErrorAsync(tokenMissingError, HttpStatusCode.Forbidden);
                return;
            }

            var token = context.Request.Headers[HeaderNames.Authorization].ToString().Replace(BearerTokenPrefix, string.Empty);

            var routeValues = context.Request.RouteValues;
            var path = $"{routeValues[ControllerRouteKey]}:{routeValues[ActionRouteKey]}";

            context.Request.Headers.TryGetValue(CorrelationIdHeader, out var correlationId);
            var isTokenValid = await _permissionCheckService.HasAccess(token, path, correlationId);

            if (!isTokenValid)
            {
                var tokenInvalidError = new ApiError(description: "Access denided.", code: ErrorType.Forbidden);
                await context.WriteErrorAsync(tokenInvalidError, HttpStatusCode.Forbidden);
                return;
            }

            await _next.Invoke(context);
        }
    }
}
